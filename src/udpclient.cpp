 /*
 UDP client class

 Licensed under the MIT License <http://opensource.org/licenses/MIT>.
 SPDX-License-Identifier: MIT
 Copyright (c) 2018 Christoph Stein <christoph@oss.impulse9.de>

 See COPYING for full license.
 */
//-----------------------------------------------------------------------------------------
#include "udpclient.h"
#include <iostream>
//-----------------------------------------------------------------------------------------
//Local definitions
#define CLIENT_BUFF_SIZE 15000
//-----------------------------------------------------------------------------------------
libsock::udp::CUDPClient::~CUDPClient() = default;
//-----------------------------------------------------------------------------------------
/*!
 * \brief libsock::udp::CUDPClient implementation details
 */
class libsock::udp::CUDPClient::cImpl
{
    public:
        struct addrinfo hints;
        struct addrinfo *res = nullptr;
        struct addrinfo *ressave = nullptr;
};

//-----------------------------------------------------------------------------------------
/*!
 * \details Default constructor
 */
libsock::udp::CUDPClient::CUDPClient() : pImpl{std::unique_ptr<cImpl>(new cImpl)}
{

}
//-----------------------------------------------------------------------------------------
/*!
 * \details Constructor taking a socket descriptor, see SetSocketDescriptor()
 * \param sockfd Socket descriptor. It is assumed the socket is already connected.
 */
libsock::udp::CUDPClient::CUDPClient(int sockfd) : pImpl{std::unique_ptr<cImpl>(new cImpl)}
{
    SetSocketDescriptor(sockfd);
}
//-----------------------------------------------------------------------------------------
/*!
 * \details Receive on port, blocking
 * \param port  Port to bind to
 * \param buffer  buffer to put received data into
 * \param buffersize Size of <buffer>
 * \param remoteaddress string where the address of the remote client will be stored
 * \param remoteport int where the port of the remote client will be stored
 * \param family Pass AF_INET or AF_INET6 to force IPv4 or IPv6.
 * \return Number of bytes received, -1 on error
 */
int libsock::udp::CUDPClient::RecvFrom(const std::string &port,
                                       char *buffer,
                                       unsigned int buffersize,
                                       std::string &remoteaddress,
                                       unsigned int &remoteport,
                                       int family)
{
    struct addrinfo hints;
    struct addrinfo *res = nullptr;
    struct addrinfo *ressave = nullptr;
    char cremote[INET6_ADDRSTRLEN];
    struct sockaddr_storage cremote_addr;
    socklen_t addr_len = sizeof(cremote_addr);

    if ((family != AF_INET) && (family != AF_INET6) && (family != AF_UNSPEC)) return false;

    memset(&(hints),0,sizeof(struct addrinfo));

    hints.ai_family = family;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_flags = AI_PASSIVE;

    int n = getaddrinfo(NULL, port.c_str(), &(hints), &(res));
    if(n) return -1;
    ressave = res;
    int s = -1;

    while(res)
    {
        s = socket(res->ai_family, res->ai_socktype, res->ai_protocol);

        if ((s > 0) && (bind(s, res->ai_addr, res->ai_addrlen) == 0)) break;
        close(s);
        s = -1;
        res = res->ai_next;
    }
    if (s < 0) return -1;
    freeaddrinfo(ressave);

    int numbytes;
    if ((numbytes = recvfrom(s, buffer, buffersize , 0,
        (struct sockaddr *)&cremote_addr, &addr_len)) == -1)
        {
            return -1;
        }

    void *paddr;
    if (((struct sockaddr*)&cremote_addr)->sa_family == AF_INET) paddr = &(((struct sockaddr_in*)&cremote_addr)->sin_addr);
    else paddr =  &(((struct sockaddr_in6*)&cremote_addr)->sin6_addr);
    inet_ntop(cremote_addr.ss_family, paddr, cremote, sizeof cremote);
    remoteaddress = std::string(cremote);
    remoteport = ntohs (((struct sockaddr_in*)&cremote_addr)->sin_port);

    close(s);
    return numbytes;
}
//-----------------------------------------------------------------------------------------
/*!
 * \details Send UDP packet, blocking
 * \param hostname  Remote host
 * \param port  Remote port
 * \param buffer  buffer to send
 * \param buffersize Size of <buffer>
 * \param family Pass AF_INET or AF_INET6 to force IPv4 or IPv6.
 * \return Number of bytes received, -1 on error
 */
int libsock::udp::CUDPClient::SendTo(const std::string& hostname,
                                     const std::string &port,
                                     const char *buffer,
                                     unsigned int buffersize,
                                     int family)
{
    struct addrinfo hints;
    struct addrinfo *res = nullptr;
    struct addrinfo *ressave = nullptr;

    if ((family != AF_INET) && (family != AF_INET6) && (family != AF_UNSPEC)) return false;
    memset(&(hints),0,sizeof(struct addrinfo));

    hints.ai_family = family;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_flags = AI_PASSIVE;

    int n = getaddrinfo(hostname.c_str(), port.c_str(), &(hints), &(res));

    if(n) return -1;
    ressave = res;
    int s = -1;

    while(res)
    {
        s = socket(res->ai_family, res->ai_socktype, res->ai_protocol);

        if (s > 0) break;
        close(s);
        s = -1;
        res = res->ai_next;
    }
    if (s < 0) return -1;

    int numbytes = sendto(s, buffer, buffersize, 0, res->ai_addr, res->ai_addrlen);
    freeaddrinfo(ressave);
    close(s);
    return numbytes;
}
//-----------------------------------------------------------------------------------------
/*!
 * \details Connect to host, for connected udp sockets.
 * \param hostname Hostname or ip-adress of host
 * \param port  Port to connect to
 * \param family Pass AF_INET or AF_INET6 to force IPv4 or IPv6.
 * \return true: Connected to host, false: error on connect or already connected
 */
bool libsock::udp::CUDPClient::ConnectToHost(const std::string& hostname,
                                             const std::string& port,
                                             int family)
{
    if (IsConnected()) return false;
    if ((family != AF_INET) && (family != AF_INET6) && (family != AF_UNSPEC)) return false;

    memset(&(pImpl->hints),0,sizeof(struct addrinfo));

    pImpl->hints.ai_family = family;
    pImpl->hints.ai_socktype = SOCK_DGRAM;

    int n = getaddrinfo(hostname.c_str(),
                        port.c_str(),
                        &(pImpl->hints),
                        &(pImpl->res));

    if(n) return false;

    pImpl->ressave = pImpl->res;
    int s = -1;

    while(pImpl->res)
    {
        s = socket(pImpl->res->ai_family,pImpl->res->ai_socktype,pImpl->res->ai_protocol);

        if(s > 0)
        {
            if(connect(s,(struct sockaddr*)pImpl->res->ai_addr,pImpl->res->ai_addrlen) == 0)
            {
                SetSocketDescriptor(s);
                freeaddrinfo(pImpl->ressave);
                return true;
            }
            close(s);
            SetSocketDescriptor(-1);
        }
        pImpl->res = pImpl->res->ai_next;
    }

    freeaddrinfo(pImpl->ressave);

    return false;
}
//-----------------------------------------------------------------------------------------
