 /*
 Unix domain socket server class

 Licensed under the MIT License <http://opensource.org/licenses/MIT>.
 SPDX-License-Identifier: MIT
 Copyright (c) 2018 Christoph Stein <christoph@oss.impulse9.de>.

 See COPYING for full license.
 */
//-----------------------------------------------------------------------------------------
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <sstream>
#include <string>
#include <thread>
#include <mutex>
#include <memory>
#include <functional>
#include <algorithm>
#include <unistd.h>
#include <netdb.h>
#include <sys/param.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/select.h>
#include <sys/fcntl.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "udsserver.h"
#include "cthreadpool.h"
//-----------------------------------------------------------------------------------------
/*!
 * \brief libsock::uds::CUDSServer implementation details
 */
class libsock::uds::CUDSServer::cImpl
{
    public:

		int nSocket;
    std::string sPath;
		struct sockaddr_un tAddr;

		std::function<void(int)>tClientCallback;
		std::function<void(const std::string&)>tLogCallback;

		void SetLastError(const std::string &a_sError);
		std::string sLasterror;

    void LogMessage(const std::string &a_sMsg);
    void LogAccepted(int a_nSocket);
    //mutex to lock exclusive access to logging function
    std::mutex LogMutex;

    void StartClientThread(int socket);

    std::unique_ptr<threading::cThreadpool> pThreadpool;
    unsigned int nNumThreads;
};
//-----------------------------------------------------------------------------------------
void libsock::uds::CUDSServer::cImpl::StartClientThread(int socket)
{
    std::string Message;
    if (pThreadpool->GetNumActiveJobs() >= nNumThreads)
    {
      LogMessage("Dropping connection (maximum number of clients reached) ");
      close (socket);
      return;
    }

    if (pThreadpool->AddJob(std::bind(tClientCallback, socket)))
    {
      LogAccepted(socket);
    }
    else
    {
      LogMessage("Error starting client thread");
      close (socket);
    }
}
//-----------------------------------------------------------------------------------------
void libsock::uds::CUDSServer::cImpl::SetLastError(const std::string &a_sError)
{
	sLasterror = a_sError;
}
//-----------------------------------------------------------------------------------------
void libsock::uds::CUDSServer::cImpl::LogMessage(const std::string &a_sMsg)
{
  if (tLogCallback)
  {
      std::string FullLogMessage;

      //current time
      std::time_t currenttime = std::time(NULL);
      struct std::tm *slocaltime = std::localtime(&currenttime);
      FullLogMessage.append(std::asctime(slocaltime));
      while (!FullLogMessage.empty() &&
             ((FullLogMessage.back() == '\n') || (FullLogMessage.back() == '\r')))
      {
          FullLogMessage.pop_back();
      }
      FullLogMessage.append(": ");
      FullLogMessage.append(a_sMsg);
      std::lock_guard<std::mutex> tLock(LogMutex);
      tLogCallback(FullLogMessage);
  }
}
//-----------------------------------------------------------------------------------------
void libsock::uds::CUDSServer::cImpl::LogAccepted(int a_nSocket)
{
  if (!tLogCallback) return;

  // Get peer information
  struct ucred tUcred;
  socklen_t nLen = sizeof(struct ucred);
  if (getsockopt(a_nSocket, SOL_SOCKET, SO_PEERCRED, &tUcred, &nLen) == -1) return;

  // Compose log message
  std::string sMsg;
  sMsg.append("New client: pid(");
  sMsg.append(std::to_string(tUcred.pid));
  sMsg.append("), uid(");
  sMsg.append(std::to_string(tUcred.uid));
  sMsg.append("), gid(");
  sMsg.append(std::to_string(tUcred.gid));
  sMsg.append(")");
  LogMessage(sMsg);
}
//-----------------------------------------------------------------------------------------
libsock::uds::CUDSServer::CUDSServer(std::string a_sPath,
													  std::function<void(int)>a_tClientCallback,
													  std::function<void(const std::string&)>a_tLogCallback,
                            unsigned int numthreads) : pImpl{std::unique_ptr<cImpl>(new cImpl)}
{
	//***
	pImpl->tAddr.sun_family = AF_LOCAL;
  pImpl->sPath = a_sPath;
	strcpy(pImpl->tAddr.sun_path, a_sPath.c_str()); //***TODO: strncpy

	pImpl->tClientCallback = a_tClientCallback;
	pImpl->tLogCallback = a_tLogCallback;

  pImpl->nNumThreads = numthreads;
  pImpl->pThreadpool = std::make_unique<threading::cThreadpool>(numthreads, numthreads);
}

//-----------------------------------------------------------------------------------------
/*!
 * \details
 * Destructor..
 */
libsock::uds::CUDSServer::~CUDSServer()
{
	close (pImpl->nSocket);
}

//-----------------------------------------------------------------------------------------
bool libsock::uds::CUDSServer::Listen()
{
  socklen_t naddrlen;
  naddrlen = sizeof(struct sockaddr_un);

  // Create socket
  pImpl->nSocket = socket(pImpl->tAddr.sun_family, SOCK_STREAM, 0);

	if (pImpl->nSocket == -1)
	{
		pImpl->SetLastError("socket");
		return false;
	}

  // Delete socket file if already present
  unlink(pImpl->tAddr.sun_path);

  // Bind socket to file
  if (bind (pImpl->nSocket,
            reinterpret_cast<struct sockaddr *>(&(pImpl->tAddr)),
            sizeof(struct sockaddr_un)) != 0)
  {
    // Error
    pImpl->SetLastError("bind");
		return false;
  }

  // listen
  if (listen(pImpl->nSocket, 5) == -1)
  {
    pImpl->SetLastError("listen");
    return false;
  }
  pImpl->LogMessage("Listening on " + pImpl->sPath);

  // accept loop
  while (1)
  {
    // accept connection
    int nClientSocket = accept(pImpl->nSocket,
                               reinterpret_cast<struct sockaddr *>(&(pImpl->tAddr)),
                               &naddrlen);
    if (nClientSocket > 0)
    {
      // start client thread
      try
      {
          pImpl->StartClientThread(nClientSocket);
      }
      catch (std::exception &e)
      {
          pImpl->LogMessage("Clientthread failed");
          continue;
      }
    }
  }
  return true;
}
//-----------------------------------------------------------------------------------------
