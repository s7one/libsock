/*
Threadpool class

Licensed under the MIT License <http://opensource.org/licenses/MIT>.
SPDX-License-Identifier: MIT
Copyright (c) 2018 Christoph Stein <christoph@oss.impulse9.de>

See COPYING for full license.
*/
//------------------------------------------------------------------------------------------
#ifndef CTHREADPOOL_HPP
#define CTHREADPOOL_HPP

#include <string>
#include <memory>
#include <functional>
//------------------------------------------------------------------------------------------
namespace threading
{
  class cThreadpool
  {
    public:
      cThreadpool() = delete;
      cThreadpool(unsigned int a_nMaxQueuesize, unsigned int a_nNumThreads = 0);
      ~cThreadpool();

      bool AddJob(std::function<void()> a_tJob);

      unsigned int GetNumActiveJobs();
      unsigned int GetNumQueuedJobs();

    private:
      cThreadpool(cThreadpool const&)        = delete;
      void operator=(cThreadpool const&)     = delete;

      class cImpl;
      std::unique_ptr<cImpl> pImpl;
  };

} //namespace
//------------------------------------------------------------------------------------------
#endif
