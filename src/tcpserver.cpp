 /*
 TCP server class

 Licensed under the MIT License <http://opensource.org/licenses/MIT>.
 SPDX-License-Identifier: MIT
 Copyright (c) 2018 Christoph Stein <christoph@oss.impulse9.de>

 See COPYING for full license.
 */
//-----------------------------------------------------------------------------------------
#include <memory>
#include <functional>
#include "tcpserver.h"
#include "cthreadpool.h"
//-----------------------------------------------------------------------------------------
/*!
 * \brief libsock::tcp::CTCPServer implementation details
 */
class libsock::tcp::CTCPServer::cImpl
{
    public:
        virtual ~cImpl() { }

        void ShutdownServer(const std::string& sError = std::string());
        void CloseServerSocket();
        bool IsServerRunning();
        void SetLastError(const std::string& sError);
        void SetProto(int family = AF_UNSPEC);
        void LogMessage(std::string Message);

        bool OpenIntPipe();
        void CloseIntPipe();

        virtual void StartClientThread(int socket, std::string clientaddr);

        //mutex to lock exclusive access to logging function
        std::mutex LogMutex;

        //low level socket internals
        int servsocket, family;
        int ap_readfd = -1;
        int ap_writefd = -1;
        std::string service, lasterror;

        //function objects for client threads and logging
        std::function<void(int clientsocket, std::string host)> tclientcallback;
        std::function<void(const std::string& Message)> tlogcallback;

        //callbacks
        std::function<void()> tserverstartedcallback;
        std::function<void()> tserverstoppedcallback;

        //server thread
        std::thread *pListenThread = nullptr;

        libsock::tcp::CTCPServer *pParent;

        std::unique_ptr<threading::cThreadpool> pThreadpool;
        unsigned int nNumThreads;
};

//-----------------------------------------------------------------------------------------
/*!
 * \details
 * Constructor, initializing the server object and making it ready for a call to liste().
 * \param port Port to listen on
 * \param tclientcallback
 * Function object of client callback. For every new connection, a separate thread will be
 * started and this callback will be executed. Takes the accepted socket descriptor and
 * the hostname as argument.
 * \param tlogcallback
 * Function object of logmessage callback. This is only used by the server itself to
 * print log/debug messages about new client connection etc. Takes the message to be
 * printed as an argument. This has not to be re-entrant as the server ensures it is only
 * used by one thread at a time.
 * \param proto Leave at default to allow IPv4 and IPv6 connections
 */
libsock::tcp::CTCPServer::CTCPServer(std::string port,
                                     std::function<void(int, std::string)>tclientcallback,
                                     std::function<void(const std::string&)>tlogcallback,
                                     unsigned int numthreads,
                                     int proto) : pImpl{std::unique_ptr<cImpl>(new cImpl)}
{
    pImpl->SetProto(proto);
    pImpl->tclientcallback = tclientcallback;
    pImpl->tlogcallback = tlogcallback;
    pImpl->service.assign(port);
    pImpl->lasterror.clear();
    pImpl->LogMutex.unlock();
    pImpl->ap_readfd = -1;
    pImpl->ap_writefd = -1;
    pImpl->servsocket = -1;
    pImpl->pParent = this;
    pImpl->nNumThreads = numthreads;
    pImpl->pThreadpool = std::make_unique<threading::cThreadpool>(numthreads, numthreads);
}

//-----------------------------------------------------------------------------------------
/*!
 * \details
 * Destructor..
 */
libsock::tcp::CTCPServer::~CTCPServer()
{
    StopListenThread();
    pImpl->CloseIntPipe();
}
//-----------------------------------------------------------------------------------------
/*!
 * \details Start the server in a separate thread
 * \return true: Server thread started, false: error while creating thread or already started.
 */
bool libsock::tcp::CTCPServer::StartListenThread()
{
    if (pImpl->pListenThread) return false;

    try
    {
        pImpl->pListenThread = new std::thread([&](){ this->Listen();});
    }
    catch (std::system_error &tError)
    {
        return false;
    }
    return true;
}
//-----------------------------------------------------------------------------------------
/*!
 * \details Stop the listening thread
 * \return true: thread stopped, false: error while stopping or no thread started.
 */
bool libsock::tcp::CTCPServer::StopListenThread()
{
    if (!(pImpl->pListenThread)) return false;

    int nHandle = GetInterruptHandle();
    if (nHandle < 0) return false;
    if (!SendInterruptToInstance(nHandle)) return false;

    pImpl->pListenThread->join();
    delete pImpl->pListenThread;
    pImpl->pListenThread = nullptr;

    return true;
}
//-----------------------------------------------------------------------------------------
/*!
 * \details Starts listening on the specified socket. This function only returns on error!
 * \return false: Error starting server, see GetLastError() for details
 */
bool libsock::tcp::CTCPServer::Listen()
{
    struct addrinfo hints, *servinfo, *servit;
    struct sockaddr_storage clientaddr;
    fd_set readfd;
    char clientaddr_string[INET6_ADDRSTRLEN];
    socklen_t clientaddr_size = sizeof(struct sockaddr_storage);
    int yes = 1, clientsocket;
    struct linger ling;
    ling.l_onoff = 1;
    ling.l_linger = 0;

    if (!(pImpl->tclientcallback) ||  pImpl->service.empty())
    {
         pImpl->SetLastError("Client callback or port not specified.");
        return false;
    }

    //Check for already running
    if (pImpl->IsServerRunning()) return false;

    //(re)-create pipe for shutdown signal
    pImpl->CloseIntPipe();
    if (!pImpl->OpenIntPipe())
    {
        pImpl->SetLastError("shutdown signal pipe creation failed");
        return false;
    }

    //***TODO
    memset(&hints,0,sizeof(struct addrinfo));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    if (getaddrinfo(NULL,  pImpl->service.c_str(), &hints, &servinfo))
    {
        pImpl->SetLastError("getaddrinfo failed");
        return false;
    }

    servit = servinfo;

    while (servit)
    {
        pImpl->servsocket = socket(servit->ai_family, servit->ai_socktype, servit->ai_protocol);
        if (pImpl->servsocket != -1)
        {
            //set server socket to non blocking mode
            if (fcntl(pImpl->servsocket, F_SETFL, O_NONBLOCK) != 0)
            {
                pImpl->ShutdownServer("fcntl failed");
                return false;
            }

            if ((setsockopt(pImpl->servsocket, SOL_SOCKET, SO_REUSEADDR, &yes,    sizeof (int)) != 0) ||
                (setsockopt(pImpl->servsocket, SOL_SOCKET, SO_LINGER,    &ling,   sizeof(ling))  != 0))
            {
              pImpl->ShutdownServer("setsockopt failed");
              return false;
            }

            //bind to address
            if (bind(pImpl->servsocket, servit->ai_addr, servit->ai_addrlen) != -1)
            {
                break;
            }
        }
        else
        {
            pImpl->SetLastError("socket failed");
            return false;
        }
        servit = servit->ai_next;
    }

    if (servit == NULL)
    {
        pImpl->ShutdownServer("bind failed");
        return false;
    }

    freeaddrinfo(servinfo);

    //start listening
    if (listen(pImpl->servsocket, DEF_MAXPENDCONNECTIONS) == -1)
    {
        pImpl->ShutdownServer("listen failed");
        return false;
    }

    //execute on-listen callback
    if (pImpl->tserverstartedcallback) pImpl->tserverstartedcallback();

    //accept loop
    pImpl->LogMessage("Server listening on port " + pImpl->service);
    while (1)
    {
        clientaddr_size = sizeof(clientaddr);

        //***TEST
        //Since the accept() call blocks, it is difficult to stop a running server. To
        //overcome this we can use GetInterruptHandle() to create a pipe. If done, we
        //do a select on the pipe AND the listening server fd without any timeouts. So
        //the select call blocks instead of the accept. To stop the running server just
        //use SendInterruptToInstance(). This writes a byte to the pipe, and if the
        //select comes out with a read-ready pipe we know someone wants to shut down.
        FD_ZERO(&readfd);
        FD_SET(pImpl->servsocket, &readfd);
        if (pImpl->ap_readfd >= 0) FD_SET(pImpl->ap_readfd, &readfd);
        if (select(std::max(pImpl->servsocket, pImpl->ap_readfd) + 1, &readfd,NULL, NULL, NULL) < 0)
        {
            pImpl->ShutdownServer("select failed");
            return false;
        }

        if ((pImpl->ap_readfd >= 0) && (FD_ISSET(pImpl->ap_readfd, &readfd)))
        {
            pImpl->LogMessage("Shutting down server due to interrupt signal");
            pImpl->ShutdownServer();
            //execute on-exit callback
            if (pImpl->tserverstoppedcallback) pImpl->tserverstoppedcallback();
            return true;
        }
        //***

        //accept incoming connection
        clientsocket = accept(pImpl->servsocket, (struct sockaddr *)&clientaddr, &clientaddr_size);

        if (clientsocket == -1) continue;

        if (((struct sockaddr *)&clientaddr)->sa_family == AF_INET)
        {
            inet_ntop(AF_INET,
                      &(((struct sockaddr_in *)&clientaddr)->sin_addr),
                      clientaddr_string,
                      sizeof(clientaddr_string));
        }
        else if (((struct sockaddr *)&clientaddr)->sa_family == AF_INET6)
        {
            inet_ntop(AF_INET6,
                      &(((struct sockaddr_in6 *)&clientaddr)->sin6_addr),
                      clientaddr_string,
                      sizeof(clientaddr_string));
        }
        else
        {
            strncpy(clientaddr_string, "Unknown", sizeof(clientaddr_string));
        }

        //start client thread
        try
        {
            pImpl->StartClientThread(clientsocket,
                                     std::string(clientaddr_string));
        }
        catch (std::exception &e)
        {
            pImpl->SetLastError("Clientthread failed");
            continue;
        }
    }

    return true;
}
//-----------------------------------------------------------------------------------------
/*!
 * \details Returns the latest occured error.
 * \return Error string
 */
std::string libsock::tcp::CTCPServer::GetLastError() const
{
    return pImpl->lasterror;
}
//-----------------------------------------------------------------------------------------
/*!
 * \details
 * Returns a handle (ipc pipe socket fd) which allows to shutdown a running server from
 * another thread.
 * \return handle to use with SendInterruptToInstance(). -1 on error.
 */
int libsock::tcp::CTCPServer::GetInterruptHandle()
{
    //return write fd as handle
    return pImpl->ap_writefd;
}
//-----------------------------------------------------------------------------------------
/*!
 * \details Shutdown running server which blocks on listen().
 * \param handle Handle received from GetInterruptHandle().
 * \return true: shutdown interrupt signal sent. false: error on sending.
 */
bool libsock::tcp::CTCPServer::SendInterruptToInstance(int handle)
{
    char dummy = 0;
    if (handle >= 0)
    {
        if (write(handle, &dummy, sizeof(char)) <= 0) return false;
    }
    return true;
}
//-----------------------------------------------------------------------------------------
/*!
 * \details Starts a new client thread for a freshly accepted connection
 * \param socket Socket descriptor
 * \param clientaddr Remote hostname
 */
void libsock::tcp::CTCPServer::cImpl::StartClientThread(int socket, std::string clientaddr)
{
    std::string Message;
    if (pThreadpool->GetNumActiveJobs() >= nNumThreads)
    {
      Message.append("Dropping connection from ");
      Message.append(clientaddr);
      Message.append(" (maximum number of clients reached) ");
      LogMessage(Message);
      close (socket);
      return;
    }

    if (pThreadpool->AddJob(std::bind(tclientcallback, socket, clientaddr)))
    {
      Message.append("Connection accepted from ");
      Message.append(clientaddr);
    }
    else
    {
      Message.append("Dropping connection from ");
      Message.append(clientaddr);
      Message.append(" (error starting client thread) ");
      close (socket);
    }

    LogMessage(Message);
}
//-----------------------------------------------------------------------------------------
/*!
 * \details Set protocol family
 * \param family AF_INET, AF_INET6 or AF_UNSPEC to allow both IPv4 and IPv6
 */
void libsock::tcp::CTCPServer::cImpl::SetProto(int family)
{
    if ((family != AF_INET6) && (family != AF_INET)) this->family = AF_UNSPEC;
    else                                             this->family = family;
}
//-----------------------------------------------------------------------------------------
/*!
 * \details Create a log message to pass to the user supplied logging function.
 * \param Message Logmessage
 */
void libsock::tcp::CTCPServer::cImpl::LogMessage(std::string Message)
{
    if (tlogcallback)
    {
        std::string FullLogMessage;

        //current time
        std::time_t currenttime = std::time(NULL);
        struct std::tm *slocaltime = std::localtime(&currenttime);

        FullLogMessage.append(std::asctime(slocaltime));
        while (!FullLogMessage.empty() &&
               ((FullLogMessage.back() == '\n') || (FullLogMessage.back() == '\r')))
        {
            FullLogMessage.pop_back();
        }
        FullLogMessage.append(": ");
        FullLogMessage.append(Message);
        std::lock_guard<std::mutex> tLock(LogMutex);
        tlogcallback(FullLogMessage);
    }
}
//-----------------------------------------------------------------------------------------
/*!
 * \details Sets a textual description of the last occured error.
 * \param sError Error message
 */
void libsock::tcp::CTCPServer::cImpl::SetLastError(const std::string& sError)
{
    lasterror.assign(sError);
}
//-----------------------------------------------------------------------------------------
/*!
 * \details Shutdown a running server and set error.
 * \param sError Error message. Empty string means: no error.
 */
void libsock::tcp::CTCPServer::cImpl::ShutdownServer(const std::string &sError)
{
    CloseServerSocket();
    CloseIntPipe();

    SetLastError(sError);
}
//-----------------------------------------------------------------------------------------
/*!
 * \details check if the server is ín listening state.
 * \return true: Server listening (Listen() blocks). false: server not listening.
 */
bool libsock::tcp::CTCPServer::cImpl::IsServerRunning()
{
    if (servsocket == -1) return false;
    else                  return true;
}
//-----------------------------------------------------------------------------------------
/*!
 * \details Closes the server listen socket if it is open
 */
void libsock::tcp::CTCPServer::cImpl::CloseServerSocket()
{
    if (servsocket != -1)
    {
        shutdown(servsocket, SHUT_RDWR);
        close(servsocket);
        servsocket = -1;
    }
}
//-----------------------------------------------------------------------------------------
/*!
 * \details
 * Creates the internal ipc pipe which allows tu shutdown a running server from another
 * thread.
 * \return true: pipe created, false: error creating pipe
 */
bool libsock::tcp::CTCPServer::cImpl::OpenIntPipe()
{
    //open ipc pipe and store file descriptors for self-messaging
    int tpipe[2];

    if (pipe(tpipe) != 0) return false;

    ap_readfd =  tpipe[0];
    ap_writefd = tpipe[1];
    return true;
}
//-----------------------------------------------------------------------------------------
/*!
 * \details Closes the internal ipc pipe if there is one.
 */
void libsock::tcp::CTCPServer::cImpl::CloseIntPipe()
{
    //close file descriptors if open
    if (ap_readfd >= 0)
    {
        close(ap_readfd);
        ap_readfd = -1;
    }
    if (ap_writefd >= 0)
    {
        close(ap_writefd);
        ap_writefd = -1;
    }
}
//-----------------------------------------------------------------------------------------
/*!
 * \details Set callback function to execute when server enters listen state
 * \param tstartedcallback callback
 */
void libsock::tcp::CTCPServer::SetStartedCallback(std::function<void()>tstartedcallback)
{
    pImpl->tserverstartedcallback = tstartedcallback;
}

//-----------------------------------------------------------------------------------------
/*!
 * \details Set callback function to execute when server exits listen state
 * \param tstoppedcallback callback
 */
void libsock::tcp::CTCPServer::SetStoppedCallback(std::function<void()>tstoppedcallback)
{
    pImpl->tserverstoppedcallback = tstoppedcallback;
}
//-----------------------------------------------------------------------------------------
