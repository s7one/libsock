/*
 Unix domain socket client class

 Licensed under the MIT License <http://opensource.org/licenses/MIT>.
 SPDX-License-Identifier: MIT
 Copyright (c) 2018 Christoph Stein <christoph@oss.impulse9.de>

 See COPYING for full license.
 */
//-----------------------------------------------------------------------------------------
#include "udsclient.h"
//-----------------------------------------------------------------------------------------
//Local definitions
#define CLIENT_BUFF_SIZE 15000
//-----------------------------------------------------------------------------------------
libsock::uds::CUDSClient::~CUDSClient() = default;
//-----------------------------------------------------------------------------------------
/*!
 * \brief libsock::uds::CUDSClient implementation details
 */
class libsock::uds::CUDSClient::cImpl
{
    public:
        struct sockaddr_un tAddr;
};

//-----------------------------------------------------------------------------------------
/*!
 * \details Default constructor
 */
libsock::uds::CUDSClient::CUDSClient() : pImpl{std::unique_ptr<cImpl>(new cImpl)}
{
    pImpl->tAddr.sun_family = AF_LOCAL;
}
//-----------------------------------------------------------------------------------------
/*!
 * \details Constructor taking a socket descriptor, see SetSocketDescriptor()
 * \param sockfd Socket descriptor. It is assumed the socket is already connected.
 */
libsock::uds::CUDSClient::CUDSClient(int sockfd) : pImpl{std::unique_ptr<cImpl>(new cImpl)}
{
    SetSocketDescriptor(sockfd);
}
//-----------------------------------------------------------------------------------------
/*!
 * \details Connect to socket.
 * \param a_sPath path to uds socket
 * \return true: Connected to host, false: error on connect or already connected
 */
bool libsock::uds::CUDSClient::Connect(const std::string& a_sPath)
{
    if (IsConnected()) return false;

    // Create socket
    int s = socket (PF_LOCAL, SOCK_STREAM, 0);
    if (s <= 0) return false;

    // connect
    strcpy(pImpl->tAddr.sun_path, a_sPath.c_str()); //***TODO: strncpy
    if (connect (s,
                 reinterpret_cast<struct sockaddr *>(&(pImpl->tAddr)),
                 sizeof (struct sockaddr_un)) == 0)
   {
     SetSocketDescriptor(s);
     return true;
   }

  return false;
}
//-----------------------------------------------------------------------------------------
/*!
 * \details Get peer credentials (user id, group id, process id)
*/
bool libsock::uds::CUDSClient::GetPeerCredentials(libsock::uds::tUDSCred &a_tCred)
{
  if (!IsConnected()) return false;

  // Get peer information
  struct ucred tUcred;
  socklen_t nLen = sizeof(struct ucred);
  if (getsockopt(GetSocketDescriptor(), SOL_SOCKET, SO_PEERCRED, &tUcred, &nLen) == -1)
  {
    return false;
  }

  a_tCred.uid = (long)tUcred.uid;
  a_tCred.gid = (long)tUcred.gid;
  a_tCred.pid = (long)tUcred.pid;
  return true;
}
//-----------------------------------------------------------------------------------------
