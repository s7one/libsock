 /*
 TCP client class

 Licensed under the MIT License <http://opensource.org/licenses/MIT>.
 SPDX-License-Identifier: MIT
 Copyright (c) 2018 Christoph Stein <christoph@oss.impulse9.de>

 See COPYING for full license.
 */
//-----------------------------------------------------------------------------------------
#include "tcpclient.h"
//-----------------------------------------------------------------------------------------
//Local definitions
#define CLIENT_BUFF_SIZE 15000
//-----------------------------------------------------------------------------------------
libsock::tcp::CTCPClient::~CTCPClient() = default;
//-----------------------------------------------------------------------------------------
/*!
 * \brief libsock::tcp::CTCPClient implementation details
 */
class libsock::tcp::CTCPClient::cImpl
{
    public:
        struct addrinfo hints;
        struct addrinfo *res = nullptr;
        struct addrinfo *ressave = nullptr;
};

//-----------------------------------------------------------------------------------------
/*!
 * \details Default constructor
 */
libsock::tcp::CTCPClient::CTCPClient() : pImpl{std::unique_ptr<cImpl>(new cImpl)}
{

}
//-----------------------------------------------------------------------------------------
/*!
 * \details Constructor taking a socket descriptor, see SetSocketDescriptor()
 * \param sockfd Socket descriptor. It is assumed the socket is already connected.
 */
libsock::tcp::CTCPClient::CTCPClient(int sockfd) : pImpl{std::unique_ptr<cImpl>(new cImpl)}
{
    SetSocketDescriptor(sockfd);
}
//-----------------------------------------------------------------------------------------

/*!
 * \details Connect to host.
 * \param hostname Hostname or ip-adress of host
 * \param port  Port to connect to
 * \param family Pass AF_INET or AF_INET6 to force IPv4 or IPv6.
 * \return true: Connected to host, false: error on connect or already connected
 */
bool libsock::tcp::CTCPClient::ConnectToHost(const std::string& hostname,
                                             const std::string& port,
                                             int family)
{
    if (IsConnected()) return false;
    if ((family != AF_INET) && (family != AF_INET6) && (family != AF_UNSPEC)) return false;

    memset(&(pImpl->hints),0,sizeof(struct addrinfo));

    pImpl->hints.ai_family = family;
    pImpl->hints.ai_socktype = SOCK_STREAM;

    int n = getaddrinfo(hostname.c_str(),
                        port.c_str(),
                        &(pImpl->hints),
                        &(pImpl->res));

    if(n) return false;

    pImpl->ressave = pImpl->res;
    int s = -1;

    while(pImpl->res)
    {
        s = socket(pImpl->res->ai_family,pImpl->res->ai_socktype,pImpl->res->ai_protocol);

        if(s > 0)
        {
            if(connect(s,(struct sockaddr*)pImpl->res->ai_addr,pImpl->res->ai_addrlen) == 0)
            {
                SetSocketDescriptor(s);
                freeaddrinfo(pImpl->ressave);
                return true;
            }
            close(s);
            SetSocketDescriptor(-1);
        }
        pImpl->res = pImpl->res->ai_next;
    }

    freeaddrinfo(pImpl->ressave);

    return false;
}
//-----------------------------------------------------------------------------------------
