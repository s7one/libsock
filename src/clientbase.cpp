 /*
 client base class

 Licensed under the MIT License <http://opensource.org/licenses/MIT>.
 SPDX-License-Identifier: MIT
 Copyright (c) 2021 Christoph Stein <christoph@oss.impulse9.de>

 See COPYING for full license.
 */
//-----------------------------------------------------------------------------------------
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <string>
#include <vector>
#include <thread>
#include <mutex>
#include <functional>
#include <memory>
#include <unistd.h>
#include <netdb.h>
#include <sys/param.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "clientbase.h"
//-----------------------------------------------------------------------------------------
//Local definitions
#define CLIENT_BUFF_SIZE 15000
//-----------------------------------------------------------------------------------------
/*!
 * \brief libsock::base::client implementation details
 */
class libsock::base::client::cBaseImpl
{
    public:
        int msocket = -1;
        bool connected = false;     // Ready for connection/already connected?
};
//-----------------------------------------------------------------------------------------
/*!
 * \details Default constructor
 */
libsock::base::client::client() : pBaseImpl{std::unique_ptr<cBaseImpl>(new cBaseImpl)}
{

}
//-----------------------------------------------------------------------------------------
/*!
 * \details Constructor taking a socket descriptor, see SetSocketDescriptor()
 * \param sockfd Socket descriptor. It is assumed the socket is already connected.
 */
libsock::base::client::client(int sockfd) : pBaseImpl{std::unique_ptr<cBaseImpl>(new cBaseImpl)}
{
    SetSocketDescriptor(sockfd);
}
//-----------------------------------------------------------------------------------------
/*!
 * \details Destructor, disconnects from host if necessary
 */
libsock::base::client::~client()
{
    Disconnect();
}
//-----------------------------------------------------------------------------------------
/*!
 * \details Returns the connected state.
 * \return true if ConnectToHost() was successfull or socket descriptor was set manually.
 */
bool libsock::base::client::IsConnected() const
{
    return (pBaseImpl->connected);
}
//-----------------------------------------------------------------------------------------
/*!
 * \details
 * Checks if there is more data pending to be read. If this returns true, the Receive()
 * functions should not block.
 * \return true: Data available, false: no data available
 */
bool libsock::base::client::IsMoreDataPending()
{
    if (!(pBaseImpl->connected)) return false;

    struct timeval nowait;
    fd_set readfd;

    FD_ZERO(&readfd);
    FD_SET(pBaseImpl->msocket, &readfd);
    memset(&nowait, 0, sizeof(struct timeval));

    select(pBaseImpl->msocket + 1, &readfd, NULL, NULL, &nowait);

    if(FD_ISSET(pBaseImpl->msocket, &readfd))   return true;
    else                                        return false;
}
//-----------------------------------------------------------------------------------------
/*!
 * \details Send data to host.
 * \param data Pointer to buffer containing data to be sent.
 * \param size Number of bytes to send from data buffer.
 * \return Number of bytes sent.
 */
int libsock::base::client::Send(const char *data, int size)
{
    // send data if socket is connected. flags are set to 0, no need to send OOB data
    if(pBaseImpl->connected && (data != nullptr)) 	return send(pBaseImpl->msocket,
                                                                data,
                                                                size,
                                                                MSG_NOSIGNAL);
    else                                        return 0;
}
//-----------------------------------------------------------------------------------------
/*!
 * \details Send string to host. Convenience function.
 * \param data String to be sent.
 * \return Number of bytes sent.
 */
int libsock::base::client::Send(const std::string& data)
{
    return Send(data.c_str(), data.size());
}
//-----------------------------------------------------------------------------------------
/*!
 * \details Send data to host. More c++ like than using char pointers.
 * \param data to be sent.
 * \return Number of bytes sent.
 */
int libsock::base::client::Send(const std::vector<char>& data)
{
    return Send(data.data(), (int)data.size());
}
//-----------------------------------------------------------------------------------------
/*!
 * \details
 * Read data from the receive buffer.
 * WARNING: if there is no data in the receive buffer, this call blocks until data is
 * available.
 * \param data Pointer to buffer where received data will be written.
 * \param size Maximum number of bytes to write to the buffer.
 * \return Number of bytes written to data buffer. -1 on error.
 */
int libsock::base::client::Receive(char *data, int size)
{
    if(pBaseImpl->connected) 	return recv(pBaseImpl->msocket,data,size,MSG_NOSIGNAL);
    else                        return 0;
}
//-----------------------------------------------------------------------------------------
/*!
 * \details
 * Read data from receive buffer into a std::string. due to internal buffer sizing, this
 * call may not contain all bytes that are available. Check IsMoreDataPending().
 * \return String
 */
std::string libsock::base::client::ReceiveString()
{
    char tBuffer[CLIENT_BUFF_SIZE];

    int nNumBytesRead = Receive(tBuffer, CLIENT_BUFF_SIZE);
    if (nNumBytesRead < 0) nNumBytesRead = 0;
    return std::string(tBuffer, nNumBytesRead);
}
//-----------------------------------------------------------------------------------------
/*!
 * \details
 * Read data from receive buffer into a std::vector. due to internal buffer sizing, this
 * call may not contain all bytes that are available. Check IsMoreDataPending().
 * \return Vector containing data
 */
std::vector<char> libsock::base::client::ReceiveVector()
{
    char tBuffer[CLIENT_BUFF_SIZE];
    std::vector<char> tRes;

    int nNumBytesRead = Receive(tBuffer, CLIENT_BUFF_SIZE);
    if (nNumBytesRead < 0) nNumBytesRead = 0;
    tRes.assign(tBuffer, tBuffer + nNumBytesRead);

    return tRes;
}
//-----------------------------------------------------------------------------------------
/*!
 * \details Basically the same as Receive() but lets the caller define a timeout in seconds.
 * \param data Pointer to buffer where received data will be written.
 * \param size Maximum number of bytes to write to the buffer.
 * \param timeout Timeout in seconds.
 * \param to Pointer to bool: stores "true" of timeout occured, false otherwise.
 * \return Number of bytes read and written to buffer. May be -1 on error.
 */
int libsock::base::client::Receive_to(char *data, int size, long timeout, bool *to)
{
    struct timeval t;
    fd_set readfd;

    t.tv_usec = 0;
    t.tv_sec = timeout;
    if (to) *to = true;

    FD_ZERO(&readfd);
    FD_SET(pBaseImpl->msocket, &readfd);

    if(pBaseImpl->connected)
    {
        if(select(pBaseImpl->msocket + 1, &readfd,NULL, NULL,&t))
        {
            if (to) *to = false;
            return recv(pBaseImpl->msocket, data, size, 0);
        }
    }
    return 0;
}
//-----------------------------------------------------------------------------------------
/*!
 * \details
 * See Receive_to, convenience overload for receiving strings.
 * Due to internal buffer sizing, this call may not contain all bytes that are available.
 * Check IsMoreDataPending().
 * \param timeout Timeout in seconds.
 * \param to Pointer to bool: stores "true" of timeout occured, false otherwise.
 * \return String
 */
std::string libsock::base::client::ReceiveString_to(long timeout, bool *to)
{
    char tBuffer[CLIENT_BUFF_SIZE];

    int nNumBytesRead = Receive_to(tBuffer, CLIENT_BUFF_SIZE, timeout, to);
    if (nNumBytesRead < 0) nNumBytesRead = 0;
    return std::string(tBuffer, nNumBytesRead);
}
//-----------------------------------------------------------------------------------------
/*!
 * \details
 * See Receive_to, convenience overload for receiving std::vector.
 * Due to internal buffer sizing, this call may not contain all bytes that are available.
 * Check IsMoreDataPending().
 * \param timeout Timeout in seconds.
 * \param to Pointer to bool: stores "true" of timeout occured, false otherwise.
 * \return Vector containing data
 */
std::vector<char> libsock::base::client::ReceiveVector_to(long timeout, bool *to)
{
    char tBuffer[CLIENT_BUFF_SIZE];
    std::vector<char> tRes;

    int nNumBytesRead = Receive_to(tBuffer, CLIENT_BUFF_SIZE, timeout, to);
    if (nNumBytesRead < 0) nNumBytesRead = 0;
    tRes.assign(tBuffer, tBuffer + nNumBytesRead);

    return tRes;
}
//-----------------------------------------------------------------------------------------
/*!
 * \details Disconnect from host.
 */
void libsock::base::client::Disconnect()
{
    // close socket/connection
    if (pBaseImpl->connected)
    {
      shutdown(pBaseImpl->msocket, SHUT_RDWR);
      close(pBaseImpl->msocket);
      pBaseImpl->connected = false;
    }
}
//-----------------------------------------------------------------------------------------
/*!
 * \details Returns the underlying socket descriptor.
 * \return Socket descriptor
 */
int libsock::base::client::GetSocketDescriptor() const
{
    return pBaseImpl->msocket;
}
//-----------------------------------------------------------------------------------------
/*!
 * \details
 * Set Socket Descriptor manually
 * If you got a connected descriptor elsewhere (e.g. forked server) and want to use it
 * inside with the client class.
 * There is no check is the socket is valid and connected, this is up to you.
 * If there is already an active connection, it is terminated before switching to the new
 * socket.
 * \param s Socket descriptor
 * \return Always true
 */
bool libsock::base::client::SetSocketDescriptor(int s)
{
    if(IsConnected()) Disconnect();

    pBaseImpl->msocket = s;
    if (s >= 0) pBaseImpl->connected = true;
    return true;
}
//-----------------------------------------------------------------------------------------
/*!
 * \details
 * Get *local* internet protocol port and address used for the outbound connection. Since you often cannot know
 * which local port (and sometimes also the address) was chosen for the local socket of a
 * connection, this function can tell you. Will return false for non-ip connections.
 * \param localaddress String reference where the address will be stored
 * \param localport    Int reference where the port will be stored
 * \return true if retrieval of values was successfull, false otherwise (e.g.: not connected, or not applicable)
 */
bool libsock::base::client::GetLocalPortAndAddress(std::string &localaddress,
                                                   unsigned int &localport) const
{
    struct sockaddr_storage localaddr;
    unsigned int lport = 0;
    char laddr[INET6_ADDRSTRLEN];
    socklen_t localaddr_size = sizeof(struct sockaddr_storage);

    if (!IsConnected()) return false;

    if (getsockname(pBaseImpl->msocket, (struct sockaddr *) &localaddr, &localaddr_size) != 0)
    {
        return false;
    }

    if (((struct sockaddr *)&localaddr)->sa_family == AF_INET)
    {
        inet_ntop(AF_INET,
                  &(((struct sockaddr_in *)&localaddr)->sin_addr),
                  laddr,
                  sizeof(laddr));
        lport = ntohs(((struct sockaddr_in *)&localaddr)->sin_port);
    }
    else if (((struct sockaddr *)&localaddr)->sa_family == AF_INET6)
    {
        inet_ntop(AF_INET6,
                  &(((struct sockaddr_in6 *)&localaddr)->sin6_addr),
                  laddr,
                  sizeof(laddr));
        lport = ntohs(((struct sockaddr_in6 *)&localaddr)->sin6_port);
    }
    else
    {
        return false;
    }

    localaddress.assign(laddr);
    localport = lport;
    return true;
}
//-----------------------------------------------------------------------------------------
/*!
 * \details
 * Stream operator for sleek sending of strings, calls Send().
 * Drawback: no error reporting.
 */
libsock::base::client& operator<< (libsock::base::client &out, const std::string &tdata)
{
    out.Send(tdata);
    return out;
}
//-----------------------------------------------------------------------------------------
/*!
 * \details
 * Stream operator for sleek sending of char vectors, calls Send().
 * Drawback: no error reporting.
 */
libsock::base::client& operator<< (libsock::base::client &out, const std::vector<char> &tdata)
{
    out.Send(tdata);
    return out;
}
//-----------------------------------------------------------------------------------------
/*!
 * \details
 * Stream operator for sleek receiving of strings, calls blocking ReceiveString().
 */
libsock::base::client& operator>> (libsock::base::client &in, std::string &tbuffer)
{
    tbuffer.assign(in.ReceiveString());
    return in;
}
//-----------------------------------------------------------------------------------------
/*!
 * \details
 * Stream operator for sleek receiving of char vectors, calls blocking ReceiveVector().
 */
libsock::base::client& operator>> (libsock::base::client &in, std::vector<char> &tbuffer)
{
    std::vector<char> tTemp = in.ReceiveVector();
    tbuffer.assign(tTemp.data(), tTemp.data() + tTemp.size());
    return in;
}
//-----------------------------------------------------------------------------------------
