 /*
 TCP SSL/TLS enabled client class

 Licensed under the MIT License <http://opensource.org/licenses/MIT>.
 SPDX-License-Identifier: MIT
 Copyright (c) 2018 Christoph Stein <christoph@oss.impulse9.de>

 See COPYING for full license.
 */
//-----------------------------------------------------------------------------------------
#include <mutex>
#include <openssl/bio.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/pem.h>
#include <openssl/x509.h>
#include <openssl/x509_vfy.h>
#include "tcpsslclient.h"
//------------------------------------------------------------------------------------------
/*!
 * \brief libsock::tcp::CTCPSSLClient immplementation details
 */
class libsock::tcp::CTCPSSLClient::cImpl
{
    public:
        //call once before using any CTCPSSLClient instances
        void InitSSL();

        bool ConnectSSL();
        void CleanupSSL();
        bool InitSSLContext(const std::string sCertfile= "",
                            const std::string sKeyfile = "",
                            const std::string a_sCADir = "",
                            const std::string a_sCAFile = "",
                            bool a_bUseSystemDefaultCA = true,
                            int a_nVerifyMode = SSL_VERIFY_NONE);

        bool SetSSLCASystemDefault();
        bool SetSSLCALocations(const std::string &a_sCADir,
                               const std::string &a_sCAFile);
        bool SetSSLVerifyPeer(int a_nMode = SSL_VERIFY_PEER);



        const SSL_METHOD    *ssl_method = nullptr;
        SSL_CTX             *ssl_ctx    = nullptr;
        SSL                 *ssl        = nullptr;
        int                 ssl_lasterror = 0;

        libsock::tcp::CTCPSSLClient *pParent;
};

//------------------------------------------------------------------------------------------
#define CLIENT_BUFF_SIZE 15000

static std::mutex   ssl_init_mutex;
static bool         ssl_initialized = false;
//------------------------------------------------------------------------------------------
/*!
 * \details Default constructor
 */
libsock::tcp::CTCPSSLClient::CTCPSSLClient() : pImpl{std::unique_ptr<cImpl>(new cImpl)}
{
    SSL_load_error_strings();
    pImpl->pParent = this;
    pImpl->InitSSL();
}
//------------------------------------------------------------------------------------------
/*!
 * \details
 * Constructor taking an accepted _server_ side socket descriptor, see SetSocketDescriptor()
 * \param sockfd Socket descriptor. It is assumed the socket is already connected.
 * \param certfile Path to certificate .pem fil for server side authentication.
 * \param keyfile Path to private key .pem file for server side authentication.
 */
libsock::tcp::CTCPSSLClient::CTCPSSLClient(int socket,
                                           const std::string& certfile,
                                           const std::string& keyfile) : pImpl{std::unique_ptr<cImpl>(new cImpl)}
{
    SSL_load_error_strings();
    pImpl->pParent = this;
    pImpl->InitSSL();
    SetSocketDescriptor(socket, certfile, keyfile);
}
//------------------------------------------------------------------------------------------
/*!
 * \details Destructor, disconnects from host if necessary
 */
libsock::tcp::CTCPSSLClient::~CTCPSSLClient()
{
    ERR_free_strings();
    Disconnect();
    pImpl->CleanupSSL();
}
//------------------------------------------------------------------------------------------
/*!
 * \details Connect to host. If already connected: disconnect first.
 * \param hostname Hostname or ip-adress of host
 * \param port  Port to connect to
 * \param family Pass AF_INET or AF_INET6 to force IPv4 or IPv6.
 * \return true: Connected to host, false: error on connect
 */
bool libsock::tcp::CTCPSSLClient::ConnectToHost(const std::string& hostname,
                                       const std::string& port,
                                       int family)
{
    //make basic tcp connection
    if (!libsock::tcp::CTCPClient::ConnectToHost(hostname, port, family))
    {
        return false;
    }

    //init ssl/tls encryption
    if (!(pImpl->ConnectSSL()))
    {
        Disconnect();
        return false;
    }
    else
    {
        return true;
    }
}
//------------------------------------------------------------------------------------------
/*!
 * \details Send data to host.
 * \param data Pointer to buffer containing data to be sent.
 * \param size Number of bytes to send from data buffer.
 * \return Number of bytes sent.
 */
int libsock::tcp::CTCPSSLClient::Send(const char *data, int size)
{
    if (IsConnected() && (data != nullptr))
    {
        int nRet = SSL_write(pImpl->ssl, data, size);
        if (nRet <= 0) pImpl->ssl_lasterror = nRet;
        return nRet;
    }
    else
    {
        return 0;
    }
}
//------------------------------------------------------------------------------------------
/*!
 * \details Send string to host. Convenience function.
 * \param data String to be sent.
 * \return Number of bytes sent.
 */
int libsock::tcp::CTCPSSLClient::Send(const std::string& data)
{
    return Send(data.c_str(), data.size());
}
//-----------------------------------------------------------------------------------------
/*!
 * \details Send data to host. More c++ like than using char pointers.
 * \param data to be sent.
 * \return Number of bytes sent.
 */
int libsock::tcp::CTCPSSLClient::Send(const std::vector<char>& data)
{
    return Send(data.data(), (int)data.size());
}
//------------------------------------------------------------------------------------------
/*!
 * \details
 * Read data from the receive buffer.
 * WARNING: if there is no data in the receive buffer, this call blocks until data is
 * available.
 * \param data Pointer to buffer where received data will be written.
 * \param size Maximum number of bytes to write to the buffer.
 * \return Number of bytes written to data buffer.
 */
int libsock::tcp::CTCPSSLClient::Receive(char *data, int size)
{
    if(IsConnected() && (data != nullptr))
    {
        int nRet = SSL_read(pImpl->ssl, data, size);
        if (nRet <= 0) pImpl->ssl_lasterror = nRet;
        return nRet;
    }
    else
    {
        return 0;
    }
}
//------------------------------------------------------------------------------------------
/*!
 * \details
 * Read data from receive buffer into a std::string. due to internal buffer sizing, this
 * call may not contain all bytes that are available. Check IsMoreDataPending().
 * \return String
 */
std::string libsock::tcp::CTCPSSLClient::ReceiveString()
{
    char tBuffer[CLIENT_BUFF_SIZE];

    int nNumBytesRead = Receive(tBuffer, CLIENT_BUFF_SIZE);

    if (nNumBytesRead > 0)  return std::string(tBuffer, nNumBytesRead);
    else                    return std::string();
}
//-----------------------------------------------------------------------------------------
/*!
 * \details
 * Read data from receive buffer into a std::vector. due to internal buffer sizing, this
 * call may not contain all bytes that are available. Check IsMoreDataPending().
 * \return Vector containing data
 */
std::vector<char> libsock::tcp::CTCPSSLClient::ReceiveVector()
{
    char tBuffer[CLIENT_BUFF_SIZE];
    std::vector<char> tRes;

    int nNumBytesRead = Receive(tBuffer, CLIENT_BUFF_SIZE);
    if (nNumBytesRead < 0) nNumBytesRead = 0;
    tRes.assign(tBuffer, tBuffer + nNumBytesRead);

    return tRes;
}
//------------------------------------------------------------------------------------------
/*!
 * \details
 * Checks if there is more data pending to be read. If this returns true, the Receive()
 * functions should not block. This function only works reliably _after_ a call to
 * Receive() to check if there is still more data pending.
 * \return true: Data available, false: no data available
 */
bool libsock::tcp::CTCPSSLClient::IsMoreDataPending()
{
    if (!IsConnected() || ((pImpl->ssl) == nullptr)) return false;

    if (SSL_pending(pImpl->ssl) > 0) return true;
    else                      return false;
}
//------------------------------------------------------------------------------------------
/*!
 * \details
 * Set Socket Descriptor manually. The puropose of this function is to use an already
 * connected socket descriptor accepted from a tcp _server_, like CTCPServer. It will
 * perform a _server_ side SSL handshake.
 * If there is already an active connection, it is terminated before switching to the new
 * socket.
 * \param s Socket descriptor
 * \param certfile Path to certificate .pem fil for server side authentication.
 * \param keyfile Path to private key .pem file for server side authentication.
 * \return true: SSL handshake succeeded, false: error on handshake
 */
bool libsock::tcp::CTCPSSLClient::SetSocketDescriptor(int s,
                                             const std::string& certfile,
                                             const std::string& keyfile)
{
    if (s < 0) return false;

    //Disconnect if necessary and set socket fd
    if (IsConnected())
    {
        Disconnect();
        pImpl->CleanupSSL();
    }
    CTCPClient::SetSocketDescriptor(s);

    //Init SSL context
    if (!(pImpl->InitSSLContext(certfile, keyfile)))
    {
        Disconnect();
        return false;
    }

    //init SSL connection
    if (!SSL_set_fd(pImpl->ssl, s))
    {
        Disconnect();
        pImpl->CleanupSSL();
        return false;
    }

    int nRet = SSL_accept(pImpl->ssl);
    if (nRet <= 0)
    {
        pImpl->ssl_lasterror = nRet;
        Disconnect();
        pImpl->CleanupSSL();
        return false;
    }
    return true;
}
//------------------------------------------------------------------------------------------
/*!
 * \details return latest OpenSSL error. Mainly for debug purposes.
 * \return OpenSSL error code
 */
int libsock::tcp::CTCPSSLClient::GetLastError()
{
    return SSL_get_error(pImpl->ssl, pImpl->ssl_lasterror);
}
//------------------------------------------------------------------------------------------
std::string libsock::tcp::CTCPSSLClient::GetLastErrorString()
{
    char cBuf[500];
    ERR_error_string_n(GetLastError(), cBuf, sizeof(cBuf));

    return std::string(cBuf);
}
//------------------------------------------------------------------------------------------
/*!
 * \details Init SSL if not done yet
 */
void libsock::tcp::CTCPSSLClient::cImpl::InitSSL()
{
    ssl_init_mutex.lock();

    if (!ssl_initialized)
    {
        OpenSSL_add_ssl_algorithms();
        SSL_load_error_strings();
        ssl_initialized = true;
    }

    ssl_init_mutex.unlock();
}
//------------------------------------------------------------------------------------------
/*!
 * \details
 * Inits the SSL connection as a client. This is called after the tcp socket connection is
 * established.
 * \return true: SSL handshake completed, false: error on handshake
 */
bool libsock::tcp::CTCPSSLClient::cImpl::ConnectSSL()
{
    CleanupSSL();

    ssl_method = TLS_client_method();

    if (!InitSSLContext()) return false;

    if (!SSL_set_fd(ssl, pParent->GetSocketDescriptor()))
    {
        CleanupSSL();
        return false;
    }

    int nRet = SSL_connect(ssl);
    if (nRet <= 0)
    {
        ssl_lasterror = nRet;
        return false;
    }

    //***TODO: client certificate/private key stuff
    //***TODO: certificate verification?!
    return true;
}
//------------------------------------------------------------------------------------------
/*!
 * \details
 * Inits a new SSL context, using a certificate and matching private key file if supplied.
 * \param sCertfile Certificate .pem file to load
 * \param sKeyfile Private key .pem file to load
 * \return true: context created, false: error
 */
bool libsock::tcp::CTCPSSLClient::cImpl::InitSSLContext(const std::string sCertfile,
                                               const std::string sKeyfile,
                                               const std::string a_sCADir,
                                               const std::string a_sCAFile,
                                               bool a_bUseSystemDefaultCA,
                                               int a_nVerifyMode)
{
    ssl_method = TLS_method();

    //create context
    ssl_ctx = SSL_CTX_new(ssl_method);
    if (ssl_ctx == nullptr) return false;

    //load certificate and key if present
    if (!sCertfile.empty() && !sKeyfile.empty())
    {
        //SSL_CTX_set_ecdh_auto(ssl_ctx, 1);
        //load certificates and keys into context
        if (SSL_CTX_use_certificate_file(ssl_ctx, sCertfile.c_str(), SSL_FILETYPE_PEM) <= 0)
        {
            CleanupSSL();
            return false;
        }

        if (SSL_CTX_use_PrivateKey_file(ssl_ctx, sKeyfile.c_str(), SSL_FILETYPE_PEM) <= 0)
        {
            CleanupSSL();
            return false;
        }
    }

    //Set CA locations and verify mode
    if (!a_sCADir.empty() && !a_sCAFile.empty())
    {
        if (!SetSSLCALocations(a_sCADir, a_sCAFile)) return false;
    }
    if (a_bUseSystemDefaultCA)
    {
        if (!SetSSLCASystemDefault())  return false;
    }
    if (!SetSSLVerifyPeer(a_nVerifyMode))  return false;

    //create ssl
    ssl = SSL_new(ssl_ctx);
    if (ssl == nullptr)
    {
        CleanupSSL();
        return false;
    }

    return true;
}
//------------------------------------------------------------------------------------------
/*!
 * \details Frees the ssl context
 */
void libsock::tcp::CTCPSSLClient::cImpl::CleanupSSL()
{
    if (ssl_ctx) SSL_CTX_free(ssl_ctx);
    ssl_ctx = nullptr;

    if (ssl) SSL_free(ssl);
    ssl = nullptr;
}
//------------------------------------------------------------------------------------------
/*!
 * \details Set system default CA locations
 * \return true on success, false on failure
 */
bool libsock::tcp::CTCPSSLClient::cImpl::SetSSLCASystemDefault()
{
    if (!ssl_ctx) return false;

    return SSL_CTX_set_default_verify_paths(ssl_ctx) ? true : false;
}
//------------------------------------------------------------------------------------------
/*!
 * \details Set custom CA locations
 * \return true on success, false on failure
 */
bool libsock::tcp::CTCPSSLClient::cImpl::SetSSLCALocations(const std::string &a_sCADir,
                                                  const std::string &a_sCAFile)
{
    if (!ssl_ctx) return false;

    return SSL_CTX_load_verify_locations(ssl_ctx,
                                         a_sCADir.c_str(),
                                         a_sCAFile.c_str()) ? true : false;
}
//------------------------------------------------------------------------------------------
bool libsock::tcp::CTCPSSLClient::cImpl::SetSSLVerifyPeer(int a_nMode)
{
    if (!ssl_ctx) return false;
    SSL_CTX_set_verify(ssl_ctx, a_nMode, nullptr);
    return true;
}
//------------------------------------------------------------------------------------------
