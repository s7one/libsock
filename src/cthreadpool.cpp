/*
Threadpool class

Licensed under the MIT License <http://opensource.org/licenses/MIT>.
SPDX-License-Identifier: MIT
Copyright (c) 2018 Christoph Stein <christoph@oss.impulse9.de>

See COPYING for full license.
*/
//------------------------------------------------------------------------------------------
#include <iostream>
#include <memory>
#include <functional>
#include <thread>
#include <mutex>
#include <vector>
#include <deque>
#include <condition_variable>

#include "cthreadpool.h"

//------------------------------------------------------------------------------------------
class threading::cThreadpool::cImpl
{
  public:

    std::vector< std::unique_ptr<std::thread> > tWorkers;
    std::deque<std::function<void()> > tJobs;
    std::mutex tJobsMutex;
    std::condition_variable tJobsCondVar;
    unsigned int nMaxJobQueueSize;

    static void ThreadFunc(threading::cThreadpool *a_pThis);

    unsigned int nNumActiveWorkers;
    std::mutex tActiveWorkerCountMutex;
    void IncActiveWorkerCount();
    void DecActiveWorkerCount();
    unsigned int GetActiveWorkerCount();

    bool bTerminateWorkers;
};
//------------------------------------------------------------------------------------------
void threading::cThreadpool::cImpl::IncActiveWorkerCount()
{
  std::unique_lock<std::mutex> tActiveWorkerCountLock(tActiveWorkerCountMutex);
  nNumActiveWorkers++;
}
//------------------------------------------------------------------------------------------
void threading::cThreadpool::cImpl::DecActiveWorkerCount()
{
  std::unique_lock<std::mutex> tActiveWorkerCountLock(tActiveWorkerCountMutex);
  if (nNumActiveWorkers > 0) nNumActiveWorkers--;
}
//------------------------------------------------------------------------------------------
unsigned int threading::cThreadpool::cImpl::GetActiveWorkerCount()
{
  std::unique_lock<std::mutex> tActiveWorkerCountLock(tActiveWorkerCountMutex);
  return nNumActiveWorkers;
}
//------------------------------------------------------------------------------------------
void threading::cThreadpool::cImpl::ThreadFunc(threading::cThreadpool *a_pThis)
{
  while(!a_pThis->pImpl->bTerminateWorkers)
  {
    std::unique_lock<std::mutex> tJobsLock(a_pThis->pImpl->tJobsMutex, std::defer_lock);
    tJobsLock.lock();

    while (a_pThis->pImpl->tJobs.empty() && !a_pThis->pImpl->bTerminateWorkers) a_pThis->pImpl->tJobsCondVar.wait(tJobsLock);

    if (a_pThis->pImpl->bTerminateWorkers) return;

    auto tJob = a_pThis->pImpl->tJobs.front();
    a_pThis->pImpl->tJobs.pop_front();
    a_pThis->pImpl->IncActiveWorkerCount();
    tJobsLock.unlock();

    tJob();
    a_pThis->pImpl->DecActiveWorkerCount();
  }
}
//------------------------------------------------------------------------------------------
threading::cThreadpool::cThreadpool(unsigned int a_nMaxQueuesize, unsigned int a_nNumThreads)
{
  pImpl = std::make_unique<threading::cThreadpool::cImpl>();
  pImpl->bTerminateWorkers = false;

  unsigned int nMax = (a_nNumThreads > 0) ? a_nNumThreads : std::thread::hardware_concurrency();
  for (unsigned int i = 0; i < nMax; i++) pImpl->tWorkers.push_back(std::make_unique<std::thread>(threading::cThreadpool::cImpl::ThreadFunc, this));

  pImpl->nMaxJobQueueSize = a_nMaxQueuesize;
}
//------------------------------------------------------------------------------------------
threading::cThreadpool::~cThreadpool()
{
  pImpl->bTerminateWorkers = true;
  for (unsigned int i = 0; i < pImpl->tWorkers.size(); i++) pImpl->tJobsCondVar.notify_one();
  for (unsigned int i = 0; i < pImpl->tWorkers.size(); i++) pImpl->tWorkers[i]->join();
}
//------------------------------------------------------------------------------------------
bool threading::cThreadpool::AddJob(std::function<void()> a_tJob)
{
  std::lock_guard<std::mutex> tJobsLock(pImpl->tJobsMutex);

  //Check queue size limit
  if (pImpl->tJobs.size() >= pImpl->nMaxJobQueueSize) return false;

  //Push job into queue
  pImpl->tJobs.push_back(a_tJob);
  pImpl->tJobsCondVar.notify_one();
  return true;
}
//------------------------------------------------------------------------------------------
unsigned int threading::cThreadpool::GetNumActiveJobs()
{
  return pImpl->GetActiveWorkerCount();
}
//------------------------------------------------------------------------------------------
unsigned int threading::cThreadpool::GetNumQueuedJobs()
{
  return pImpl->tJobs.size();
}
//------------------------------------------------------------------------------------------
