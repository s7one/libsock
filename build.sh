#!/bin/bash
#
# Usage: 
# "./build.sh"        -> build and install release configuration. Installation is canceled if not running as root.
# "./build.sh debug"  -> build and install debug configuration (includes instrumentation for code coverage via .gcno files)

TYPE=${1:-Release}

# execute build in an out-of-source directory to keep the sources clean
echo "building: ${TYPE}"
mkdir -p build_${TYPE} && cd build_${TYPE}
cmake -DCMAKE_BUILD_TYPE=${TYPE} ..

# build and install library
make -j$(nproc --all)
if [ "$(id -u)" == 0 ]; then
    make install
    make package
else
    echo "Not running as root, skipping install"
fi

