# Lightweight C++ Socket Library


**Author: Christoph Stein**

**Copyright: MIT**

## About

Libsock is a rather simple and lightweight class library to wrap much of the boring boiler
plate code to fiddle with unix domain and tcp clients/servers. It is meant to provide
an easy-to-use framework when creating tcp client or server applications, it also
supports SSL/TLS enabled tcp connections.

If you are familiar with socket programming most of the functionality should be fairly
self-explanatory. For starters, here are some code snippets how the classes can be used:

### tcp server example

```
#include <libsock/tcpserver.h>
#include <libsock/tcpclient.h>

// simple echo server on tcp port 7000
libsock::tcp::CTCPServer server(std::string("7000"),
                                [](int s, const std::string& tStr)
                                {
                                  libsock::tcp::CTCPClient c(s);
                                  while(c.Send(c.ReceiveVector()));
                                },
                                [](const std::string& tStr){ std::cout << tStr << std::endl;});

//start server, will only return on error
server.Listen();

//error occured
std::cout << "Error: "<< server.GetLastError() << std::endl;
```
### tcp client example

Connects to www.example.com and trys to fetch the start page of a web sever.

```
#include <libsock/tcpclient.h>

//simple client example
libsock::tcp::CTCPClient tClient;

if (!tClient.ConnectToHost("www.example.com", "80"))
{
  std::cout << "Error: ConnectToHost" << std::endl;
  return 1;
}
std::string sRequest("GET / HTTP/1.1\r\nHost: ");
sRequest.append("www.example.com");
sRequest.append(std::string("\r\nConnection: close\r\n\r\n"));
if (static_cast<unsigned int>(tClient.Send(sRequest)) != sRequest.size())
{
  std::cout << "Error: Send" << std::endl;
  return 1;
}
std::string sAnswer = tClient.ReceiveString_to(5);
std::cout << sAnswer << std::endl;
```

----

## Build dependencies

The following dependencies are needed to build the library:

  * CMake
  * C++11 capable toolchain
  * OpenSSL 1.1 or above

  (debian/ubuntu: ```apt-get install build-essential cmake libssl-dev```)


  To build the documentation you will also need:

  * doxygen
  * graphviz

  (debian/ubuntu): ```apt-get install doxygen graphviz ttf-freefont```)

----

## How to build/install

Build and install with cmake and make:

```
mkdir build && cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make
sudo make install
```

You can also just run ```build.sh``` which will do this.

----

## How to build the documentation

The source code is documented using doxygen comments. To build the documentation just use the doxyfile in the ```doc``` subfolder:

```
cd doc
doxygen
```

## Examples

Examples on how to use the library can be found in the ```example``` subfolder. These are simple makefile projects.
