 /*
 client base class

 Licensed under the MIT License <http://opensource.org/licenses/MIT>.
 SPDX-License-Identifier: MIT
 Copyright (c) 2021 Christoph Stein <christoph@oss.impulse9.de>

 See COPYING for full license.
 */
//-----------------------------------------------------------------------------------------
#ifndef CLIENTBBASE_H
#define CLIENTBBASE_H

#include <string>
#include <vector>

namespace libsock
{
    namespace base
    {
        /*!
         * \brief base class
         * \details
         * Client base class
         */
        class client
        {
            public:
                client();
                client(int sockfd);
                virtual ~client();

                // return: true if connected, false otherwise
                virtual bool IsConnected() const;

                // return: true if more data is ready (Receive() will not block), false otherwise
                virtual bool IsMoreDataPending();

                //Send data to remote host
                virtual int Send(const char *data, int size);
                virtual int Send(const std::string &data);
                virtual int Send(const std::vector<char> &data);

                // Receive from Remote Host....blocking!!
                virtual int Receive(char *data, int size);
                virtual std::string ReceiveString();
                virtual std::vector<char> ReceiveVector();

                // Receive from Remote Host with timeout (seconds)
                virtual int Receive_to(char *data,
                                       int size,
                                       long timeout,
                                       bool* to = nullptr);
                virtual std::string ReceiveString_to(long timeout,
                                                     bool *to = nullptr);
                virtual std::vector<char> ReceiveVector_to(long timeout,
                                                           bool *to = nullptr);

                // Close Connection
                virtual void Disconnect();

                // Get Socket Descriptor, for external use
                virtual int GetSocketDescriptor() const;
                // Set Socket Descriptor manually
                virtual bool SetSocketDescriptor(int s);

                // Get *local* port and address for internet protocol sockets
                virtual bool GetLocalPortAndAddress(std::string &localaddress,
                                                    unsigned int &localport) const;

            private:
                class cBaseImpl;
                std::unique_ptr<cBaseImpl> pBaseImpl;
        };
    }
}

//Stream operators for basic send/receive with some syntactic sugar
libsock::base::client& operator<< (libsock::base::client &out, const std::string &tdata);
libsock::base::client& operator<< (libsock::base::client &out, const std::vector<char> &tdata);
libsock::base::client& operator>> (libsock::base::client &in, std::string &tbuffer);
libsock::base::client& operator>> (libsock::base::client &in, std::vector<char> &tbuffer);
//-----------------------------------------------------------------------------------------
#endif
