 /*
 TCP client class

 Licensed under the MIT License <http://opensource.org/licenses/MIT>.
 SPDX-License-Identifier: MIT
 Copyright (c) 2018 Christoph Stein <christoph@oss.impulse9.de>

 See COPYING for full license.
 */
//-----------------------------------------------------------------------------------------
#ifndef CTCPCLIENT_H
#define CTCPCLIENT_H

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <string>
#include <vector>
#include <thread>
#include <mutex>
#include <functional>
#include <memory>
#include <unistd.h>
#include <netdb.h>
#include <sys/param.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "clientbase.h"
//-----------------------------------------------------------------------------------------
namespace libsock
{
    namespace tcp
    {
        /*!
         * \brief Encapsulates a tcp client
         * \details
         * Encapsulates a tcp client, wrapping the usual BSD socket boiler plate code into
         * (hopefully) more convenient functions while retaining the most common needed
         * functionality. If a specific not supported function is needed there is always
         * the possibility to access the underlying socket descriptor.
         */
        class CTCPClient : public libsock::base::client
        {

            public:
                CTCPClient();
                CTCPClient(int sockfd);
                virtual ~CTCPClient();

                //Connect to remote host
                bool ConnectToHost(const std::string& hostname,
                                   const std::string& port,
                                   int family = AF_UNSPEC);

            private:
                class cImpl;
                std::unique_ptr<cImpl> pImpl;
        };
    }
}
//-----------------------------------------------------------------------------------------
#endif
