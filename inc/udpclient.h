 /*
 UDP client class

 Licensed under the MIT License <http://opensource.org/licenses/MIT>.
 SPDX-License-Identifier: MIT
 Copyright (c) 2018 Christoph Stein <christoph@oss.impulse9.de>

 See COPYING for full license.
 */
//-----------------------------------------------------------------------------------------
#ifndef CUDPCLIENT_H
#define CUDPCLIENT_H

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <string>
#include <vector>
#include <thread>
#include <mutex>
#include <functional>
#include <memory>
#include <unistd.h>
#include <netdb.h>
#include <sys/param.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "clientbase.h"
//-----------------------------------------------------------------------------------------
namespace libsock
{
    namespace udp
    {
        /*!
         * \brief Encapsulates a udp client
         * \details
         * Encapsulates a udp client, wrapping the usual BSD socket boiler plate code into
         * (hopefully) more convenient functions while retaining the most common needed
         * functionality. If a specific not supported function is needed there is always
         * the possibility to access the underlying socket descriptor.
         */
        class CUDPClient : public libsock::base::client
        {

            public:
                CUDPClient();
                CUDPClient(int sockfd);
                virtual ~CUDPClient();

                //Connect to remote host, for connected udp sockets
                bool ConnectToHost(const std::string& hostname,
                                   const std::string& port,
                                   int family = AF_UNSPEC);

                //Static functions for unconnected udp sockets
                static int RecvFrom(const std::string &port,
                                    char *buffer,
                                    unsigned int buffersize,
                                    std::string &remoteaddress,
                                    unsigned int &remoteport,
                                    int family = AF_UNSPEC);

                static int SendTo(const std::string& hostname,
                                  const std::string &port,
                                  const char *buffer,
                                  unsigned int buffersize,
                                  int family = AF_UNSPEC);

            private:
                class cImpl;
                std::unique_ptr<cImpl> pImpl;
        };
    }
}
//-----------------------------------------------------------------------------------------
#endif
