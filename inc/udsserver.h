 /*
 Unix domain socket server class

 Licensed under the MIT License <http://opensource.org/licenses/MIT>.
 SPDX-License-Identifier: MIT
 Copyright (c) 2018 Christoph Stein <christoph@oss.impulse9.de>

 See COPYING for full license.
 */
//-----------------------------------------------------------------------------------------
#ifndef CUDSSERVER_H
#define CUDSSERVER_H

#include <memory>
#include <functional>
//-----------------------------------------------------------------------------------------
namespace libsock
{
    namespace uds
    {
        /*!
         * \brief Encapsulates a uds server.
         */
        class CUDSServer
        {

            public:
                CUDSServer(std::string a_sPath,
    					   std::function<void(int)>tclientcallback,
    					   std::function<void(const std::string&)>tlogcallback,
                           unsigned int numthreads = 2);

                ~CUDSServer();

                bool Listen();

            private:
                class cImpl;
                std::unique_ptr<cImpl> pImpl;
        };
    }
}
//-----------------------------------------------------------------------------------------

#endif
