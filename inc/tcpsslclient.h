 /*
 TCP SSL/TLS enabled client class

 Licensed under the MIT License <http://opensource.org/licenses/MIT>.
 SPDX-License-Identifier: MIT
 Copyright (c) 2018 Christoph Stein <christoph@oss.impulse9.de>

 See COPYING for full license.
 */
//------------------------------------------------------------------------------------------
#ifndef CTCPSSLCLIENT_H
#define CTCPSSLCLIENT_H

#include "tcpclient.h"
//------------------------------------------------------------------------------------------
namespace libsock
{
    namespace tcp
    {
        /*!
         * \brief Encapsulates a TLS enabled tcp client
         * \details
         * Encapsulates a tcp client, wrapping the usual BSD socket AND OpenSSL boiler plate
         * code into (hopefully) more convenient functions while retaining the most common
         * needed functionality. If a specific not supported function is needed there is always
         * the possibility to access the underlying socket descriptor.\n
         *
         * Due to some SSL related behaviour, note that this works a bit different.
         */

        class CTCPSSLClient : public CTCPClient
        {
            public:
                CTCPSSLClient();
                CTCPSSLClient(int socket,
                              const std::string& certfile,
                              const std::string& keyfile);
                virtual ~CTCPSSLClient();

                //Connect to remote host
                bool ConnectToHost(const std::string& hostname,
                                   const std::string& port,
                                   int family = AF_UNSPEC);

                // return: true if more data is ready (Receive() will not block), false otherwise
                bool IsMoreDataPending();

                //Send data to remote host
                int Send(const char *data, int size);
                int Send(const std::string& data);
                int Send(const std::vector<char>& data);

                // Receive from Remote Host....blocking!!
                int Receive(char *data, int size);
                std::string ReceiveString();
                std::vector<char> ReceiveVector();

                // Set Socket Descriptor manually
                bool SetSocketDescriptor(int s,
                                         const std::string& certfile,
                                         const std::string& keyfile);

                int GetLastError();
                std::string GetLastErrorString();

            private:
                class cImpl;
                std::unique_ptr<cImpl> pImpl;
        };
    }
}
//------------------------------------------------------------------------------------------
#endif
