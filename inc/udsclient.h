 /*
 Unix domain socket client class

 Licensed under the MIT License <http://opensource.org/licenses/MIT>.
 SPDX-License-Identifier: MIT
 Copyright (c) 2018 Christoph Stein <christoph@oss.impulse9.de>

 See COPYING for full license.
 */
//-----------------------------------------------------------------------------------------
#ifndef CUDSCLIENT_H
#define CUDSCLIENT_H

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <string>
#include <vector>
#include <thread>
#include <mutex>
#include <functional>
#include <memory>
#include <unistd.h>
#include <netdb.h>
#include <sys/param.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/un.h>

#include "clientbase.h"
//-----------------------------------------------------------------------------------------
namespace libsock
{
  namespace uds
  {
    typedef struct
    {
      long pid;
      long uid;
      long gid;
    } tUDSCred;

      /*!
       * \brief Encapsulates a uds client
       * \details
       * Encapsulates a uds client, wrapping the usual socket boiler plate code into
       * (hopefully) more convenient functions while retaining the most common needed
       * functionality. If a specific not supported function is needed there is always
       * the possibility to access the underlying socket descriptor.
       */
    class CUDSClient : public libsock::base::client
    {
      public:
        CUDSClient();
        CUDSClient(int sockfd);
        virtual ~CUDSClient();

        //Connect to remote host
        bool Connect(const std::string& a_sPath);

        // Get peer credentials
        bool GetPeerCredentials(libsock::uds::tUDSCred &a_tCred);

      private:
        class cImpl;
        std::unique_ptr<cImpl> pImpl;
      };
  }
}
//-----------------------------------------------------------------------------------------
#endif
