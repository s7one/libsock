 /*
 TCP server class

 Licensed under the MIT License <http://opensource.org/licenses/MIT>.
 SPDX-License-Identifier: MIT
 Copyright (c) 2018 Christoph Stein <christoph@oss.impulse9.de>

 See COPYING for full license.
 */
//-----------------------------------------------------------------------------------------
#ifndef CTCPSERVER_H
#define CTCPSERVER_H

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <sstream>
#include <string>
#include <thread>
#include <mutex>
#include <memory>
#include <functional>
#include <algorithm>
#include <unistd.h>
#include <netdb.h>
#include <sys/param.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/select.h>
#include <sys/fcntl.h>
#include <netinet/in.h>
#include <arpa/inet.h>

//-----------------------------------------------------------------------------------------
#define DEF_MAXNUMTHREADS 10
#define DEF_MAXPENDCONNECTIONS 10
//-----------------------------------------------------------------------------------------
namespace libsock
{
    namespace tcp
    {
        /*!
         * \brief Encapsulates a tcp server.
         * \details
         * Encapsulates a multithreaded tcp server, wrapping the usual BSD sockets and thrading
         * boiler plate code. Offers a simple to use interface, starting a server is really
         * only a matter of constructing a CTCPServer object and call Listen(). That's it.
         */
        class CTCPServer
        {

            public:
                CTCPServer(std::string port,
                           std::function<void(int, std::string)>tclientcallback,
                           std::function<void(const std::string&)>tlogcallback,
                           unsigned int numthreads = 2,
                           int proto = AF_UNSPEC);

                ~CTCPServer();

                bool Listen();
                bool StartListenThread();
                bool StopListenThread();

                std::string GetLastError() const;

                int GetInterruptHandle();
                static bool SendInterruptToInstance(int handle);

                void SetStartedCallback(std::function<void()>tstartedcallback);
                void SetStoppedCallback(std::function<void()>tstoppedcallback);

            private:
                class cImpl;
                std::unique_ptr<cImpl> pImpl;
        };
    }
}
//-----------------------------------------------------------------------------------------

#endif
