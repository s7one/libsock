 /*
 Unit tests

 Licensed under the MIT License <http://opensource.org/licenses/MIT>.
 SPDX-License-Identifier: MIT
 Copyright (c) 2021 Christoph Stein <christoph@oss.impulse9.de>

 See COPYING for full license.
 */
//-----------------------------------------------------------------------------------------

#include <future>
#include <chrono>
#include <thread>
#include <cstdlib>
#include <iostream>
#include <string>
#include <cerrno>
#include <unistd.h>

#include "tcpclient.h"
#include "tcpsslclient.h"
#include "tcpserver.h"
#include "udsserver.h"
#include "udsclient.h"
#include "udpclient.h"
//-----------------------------------------------------------------------------------------

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
//-----------------------------------------------------------------------------------------
TEST_CASE( "Test client base", "Basic client base class functionality test" )
{
    std::cout << "Start base client unit tests" << std::endl;

    //testserver is a simple websever provided by the test environment
    std::string sHost("testserver");
    std::string sPort("8000");
    std::string sRequest("GET / HTTP/1.1\r\nHost: ");
    sRequest.append(std::string(sHost));
	sRequest.append(std::string("\r\nConnection: close\r\n\r\n"));
    std::string sAnswer;

    // Create new client from existing socket
    // Use tcp socket to connect, then use base class functions
    libsock::tcp::CTCPClient tClient;
    REQUIRE (tClient.ConnectToHost(sHost, sPort) == true);
    libsock::base::client tClientEx(tClient.GetSocketDescriptor());

    REQUIRE (tClientEx.IsConnected() == true);
    REQUIRE (tClientEx.Send(sRequest) == (int)sRequest.size());
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    REQUIRE(tClientEx.IsMoreDataPending() == true);
    //Receive and check answer
    sAnswer.clear();
	sAnswer.assign(tClientEx.ReceiveString_to(5));
    REQUIRE(sAnswer.find("HTTP") != std::string::npos);
    tClient.Disconnect();
}
//-----------------------------------------------------------------------------------------
TEST_CASE( "Test CTCPClient", "Basic cTCPClient functionality test" )
{
    std::cout << "Start CTCPClient unit tests" << std::endl;

    libsock::tcp::CTCPClient tClient;
    std::string sAnswer;
    std::vector<char> tAnswer;

    //testserver is a simple websever provided by the test environment
    std::string sHost("testserver");
    std::string sPort("8000");

    //build sample http get request as string and char vector
    std::string sRequest("GET / HTTP/1.1\r\nHost: ");
    sRequest.append(std::string(sHost));
	sRequest.append(std::string("\r\nConnection: close\r\n\r\n"));
    std::vector<char> tRequest(sRequest.begin(), sRequest.end());

    //send http request using string functions
    REQUIRE (tClient.ConnectToHost(sHost, sPort) == true);
    REQUIRE (tClient.IsMoreDataPending() == false);
    REQUIRE (tClient.Send(sRequest) == (int)sRequest.size());
    //test detection of socket state
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    REQUIRE(tClient.IsMoreDataPending() == true);
    //Receive and check answer
    sAnswer.clear();
	sAnswer.assign(tClient.ReceiveString_to(5));
    REQUIRE(sAnswer.find("HTTP") != std::string::npos);
    // Test local port and address detection
    std::string sLocalAddr;
    unsigned int nLocalPort = 0;
    REQUIRE(tClient.GetLocalPortAndAddress(sLocalAddr, nLocalPort) == true);
    std::cout << "Local address: " << sLocalAddr << ":" << nLocalPort << std::endl;

    //do the same again, using the char vector functions
    sAnswer.clear();
    tAnswer.clear();
    tClient.Disconnect();
    REQUIRE (tClient.ConnectToHost(sHost, sPort) == true);
    REQUIRE (tClient.IsMoreDataPending() == false);
    REQUIRE (tClient.Send(tRequest) == (int)tRequest.size());
    //test detection of socket state
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    REQUIRE(tClient.IsMoreDataPending() == true);
    //Receive and check answer
    tAnswer = tClient.ReceiveVector_to(5);
    sAnswer.clear();
    sAnswer.assign(tAnswer.begin(), tAnswer.end());
    REQUIRE(sAnswer.find("HTTP") != std::string::npos);

    //do the same again, using the stream operators for both string
    //and vector functions
    sAnswer.clear();
    tClient.Disconnect();
    REQUIRE (tClient.ConnectToHost(sHost, sPort) == true);
    REQUIRE (tClient.IsMoreDataPending() == false);
    tClient << sRequest;
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    REQUIRE(tClient.IsMoreDataPending() == true);
    if (tClient.IsMoreDataPending()) tClient >> sAnswer;
    REQUIRE(sAnswer.find("HTTP") != std::string::npos);

    sAnswer.clear();
    tAnswer.clear();
    tClient.Disconnect();
    REQUIRE (tClient.ConnectToHost(sHost, sPort) == true);
    tClient << tRequest;
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    REQUIRE(tClient.IsMoreDataPending() == true);
    if (tClient.IsMoreDataPending()) tClient >> tAnswer;
    sAnswer.assign(tAnswer.begin(), tAnswer.end());
    REQUIRE(sAnswer.find("HTTP") != std::string::npos);
    tClient.Disconnect();
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));

    //tests when not connected
    tClient.Disconnect();
    REQUIRE (tClient.IsConnected() == false);
    REQUIRE (tClient.IsMoreDataPending() == false);
    REQUIRE (tClient.Send(sRequest) == 0);
    REQUIRE (tClient.IsMoreDataPending() == false);
    REQUIRE (tClient.ReceiveString_to(1).empty());
    REQUIRE (tClient.ReceiveVector_to(1).empty());
}
//-----------------------------------------------------------------------------------------
TEST_CASE( "Test CTCPSSLClient", "Basic CTCPCSSLlient functionality test" )
{
    std::cout << "Start CTCPSSLClient unit tests" << std::endl;

    libsock::tcp::CTCPSSLClient tClient;
    std::string sAnswer;
    std::vector<char> tAnswer;

    //testserver_ssl is a simple https websever provided by the test environment
    std::string sHost("testserver_ssl");
    std::string sPort("443");

	  //build sample http get request as string and char vector
    std::string sRequest("GET / HTTP/1.1\r\nHost: ");
	sRequest.append(std::string(sHost));
	sRequest.append(std::string("\r\nConnection: close\r\n\r\n"));
    std::vector<char> tRequest(sRequest.begin(), sRequest.end());

    //send http request using string functions
    REQUIRE (tClient.ConnectToHost(sHost, sPort) == true);
    REQUIRE (tClient.Send(sRequest) == (int)sRequest.size());
    //test detection of socket state
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    //Receive and check answer
    sAnswer.clear();
    sAnswer.assign(tClient.ReceiveString());
    REQUIRE(sAnswer.find("HTTP") != std::string::npos);

    //do the same again, using the char vector functions
    sAnswer.clear();
    tAnswer.clear();
    tClient.Disconnect();
    REQUIRE (tClient.ConnectToHost(sHost, sPort) == true);
    REQUIRE (tClient.Send(tRequest) == (int)tRequest.size());
    //Receive and check answer
    tAnswer = tClient.ReceiveVector();
    //test detection of socket state
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    REQUIRE(tClient.IsMoreDataPending() == false);
    sAnswer.clear();
    sAnswer.assign(tAnswer.begin(), tAnswer.end());
    REQUIRE(sAnswer.find("HTTP") != std::string::npos);

    //do the same again, using the stream operators
    sAnswer.clear();
    tClient.Disconnect();
    REQUIRE (tClient.ConnectToHost(sHost, sPort) == true);
    tClient << sRequest;
    tClient >> sAnswer;
    REQUIRE(tClient.IsMoreDataPending() == false);
    REQUIRE(sAnswer.find("HTTP") != std::string::npos);

    sAnswer.clear();
    tAnswer.clear();
    tClient.Disconnect();
    REQUIRE (tClient.ConnectToHost(sHost, sPort) == true);
    tClient << tRequest;
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    tClient >> tAnswer;
    REQUIRE(tClient.IsMoreDataPending() == false);
    sAnswer.assign(tAnswer.begin(), tAnswer.end());
    REQUIRE(sAnswer.find("HTTP") != std::string::npos);

    //test failure of ssl connection with unencrypted host
    tClient.Disconnect();
    REQUIRE (tClient.ConnectToHost("testserver","8000") == false);

    //tests when not connected
    tClient.Disconnect();
    REQUIRE (tClient.IsConnected() == false);
    REQUIRE (tClient.IsMoreDataPending() == false);
    REQUIRE (tClient.Send(sRequest) == 0);
    REQUIRE (tClient.IsMoreDataPending() == false);
    REQUIRE (tClient.ReceiveString().empty());
    REQUIRE (tClient.ReceiveVector().empty());
}

//-----------------------------------------------------------------------------------------
TEST_CASE("Test CTCPServer", "Basic CTCPServer functionality test")
{
     std::cout << "Start CTCPCServer unit tests" << std::endl;

     //Test TCTPCServer
     //VERY simple echo server, accepting 2 connections at the same time on tcp port 7000
     libsock::tcp::CTCPServer server(std::string("7000"),
                                     [](int s, std::string tStr){ libsock::tcp::CTCPClient c(s); while(c.Send(c.ReceiveVector()));},
                                     [](const std::string& tStr){ std::cout << tStr << std::endl;});

     //start server
     bool bOk = server.StartListenThread();
     REQUIRE(bOk == true);
     std::this_thread::sleep_for(std::chrono::milliseconds(2000));

     //connect two clients
     std::string sAnswer;
     libsock::tcp::CTCPClient tclient;
     bOk = tclient.ConnectToHost("localhost", "7000");
     REQUIRE(bOk == true);
     bOk = tclient.ConnectToHost("localhost", "7000");
     REQUIRE(bOk == false);
     bOk = tclient.Send("test");
     REQUIRE(bOk == true);
     std::this_thread::sleep_for(std::chrono::milliseconds(2000));
     REQUIRE (tclient.IsMoreDataPending() == true);
     tclient >> sAnswer;
     REQUIRE (!sAnswer.empty());
     REQUIRE (tclient.IsMoreDataPending() == false);

     libsock::tcp::CTCPClient tclient2;
     bOk = tclient2.ConnectToHost("localhost", "7000");
     REQUIRE(bOk == true);
     REQUIRE(tclient2.Send("test") != 0);
     std::this_thread::sleep_for(std::chrono::milliseconds(2000));
     REQUIRE(tclient2.Send("test") != 0);

     //try to connect third client - will succeed at first, but the connection should get closed immediately
     libsock::tcp::CTCPClient tclient3;
     bOk = tclient3.ConnectToHost("localhost", "7000");
     REQUIRE(bOk == true);
     std::this_thread::sleep_for(std::chrono::milliseconds(2000));
     REQUIRE(tclient3.Send("test") <= 0);

     //...disconnect first client and retry connecting the third. This time it should work.
     tclient.Disconnect();
     tclient3.Disconnect();
     bOk = tclient3.ConnectToHost("localhost", "7000");
     REQUIRE(bOk == true);
     std::this_thread::sleep_for(std::chrono::milliseconds(2000));
     REQUIRE(tclient3.Send("test") != 0);

     //no disconnect...test cleanup

     // reconnect first client
     tclient3.Disconnect();
     bOk = tclient.ConnectToHost("localhost", "7000");
     REQUIRE(bOk == true);
     // send something
     std::string sMsg1("test");
     unsigned int nNumBytesSent1 = tclient.Send(sMsg1);
     REQUIRE(nNumBytesSent1 == sMsg1.size());

     //receive something
     std::string sResponse1 = tclient.ReceiveString();
     REQUIRE(!sResponse1.empty());
     REQUIRE(sResponse1 == sMsg1);

     //disconnect
     tclient.Disconnect();

     //shutdown server and immediately restart and stop again
     bOk = server.StopListenThread();
     REQUIRE(bOk == true);

     std::this_thread::yield();
     std::this_thread::sleep_for(std::chrono::milliseconds(2000));

     bOk = server.StartListenThread();
     REQUIRE(bOk == true);

     std::this_thread::yield();
     std::this_thread::sleep_for(std::chrono::milliseconds(2000));

     bOk = server.StopListenThread();
     REQUIRE(bOk == true);

     std::this_thread::yield();
     std::this_thread::sleep_for(std::chrono::milliseconds(2000));

     //Check that the server exited
     bOk = tclient.ConnectToHost("localhost", "7000");
     REQUIRE(bOk == false);
}
//-----------------------------------------------------------------------------------------
TEST_CASE( "Test Unix Domain Socket server/client", "Basic UDS functionality test" )
{
    std::cout << "Start UDS server/client unit tests" << std::endl;

    libsock::uds::CUDSClient tClient;
    libsock::uds::tUDSCred tCred;
    std::string sAnswer;
    std::vector<char> tAnswer;

    std::string sHost("./testsock.sock");
    std::string sRequest("foo bar");
    std::vector<char> tRequest(sRequest.begin(), sRequest.end());

     //VERY simple echo server, accepting 2 connections at the same time
     std::thread tUDSServerThread([sHost]()
                                  {
                                    libsock::uds::CUDSServer server(sHost,
                                                                    [](int s){ libsock::uds::CUDSClient c(s); while(c.Send(c.ReceiveVector()));},
                                                                    [](const std::string& tStr){ std::cout << tStr << std::endl;});
                                    server.Listen();
                                  });
    tUDSServerThread.detach();
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));

    // connect and peer credentials
    REQUIRE (tClient.GetPeerCredentials(tCred) == false);
    REQUIRE (tClient.Connect(sHost) == true);
    REQUIRE (tClient.IsMoreDataPending() == false);
    REQUIRE (tClient.GetPeerCredentials(tCred) == true);
    REQUIRE (tCred.uid == getuid());
    REQUIRE (tCred.gid == getgid());
    // Test local port and address detection
    std::string sLocalAddr;
    unsigned int nLocalPort = 0;
    REQUIRE(tClient.GetLocalPortAndAddress(sLocalAddr, nLocalPort) == false);
    std::cout << "Local address: " << sLocalAddr << ":" << nLocalPort << std::endl;

    //send using string functions
    REQUIRE (tClient.Send(sRequest) == (int)sRequest.size());
    //test detection of socket state
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    REQUIRE(tClient.IsMoreDataPending() == true);
    //Receive and check answer
    sAnswer.clear();
	sAnswer.assign(tClient.ReceiveString_to(5));
    REQUIRE(sAnswer.find("foo bar") != std::string::npos);

    //do the same again, using the char vector functions
    sAnswer.clear();
    tAnswer.clear();
    tClient.Disconnect();
    REQUIRE (tClient.Connect(sHost) == true);
    REQUIRE (tClient.IsMoreDataPending() == false);
    REQUIRE (tClient.Send(tRequest) == (int)tRequest.size());
    //test detection of socket state
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    REQUIRE(tClient.IsMoreDataPending() == true);
    //Receive and check answer
    tAnswer = tClient.ReceiveVector_to(5);
    sAnswer.clear();
    sAnswer.assign(tAnswer.begin(), tAnswer.end());
    REQUIRE(sAnswer.find("foo bar") != std::string::npos);

    //do the same again, using the stream operators for both string
    //and vector functions
    sAnswer.clear();
    tClient.Disconnect();
    REQUIRE (tClient.Connect(sHost) == true);
    REQUIRE (tClient.IsMoreDataPending() == false);
    tClient << sRequest;
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    REQUIRE(tClient.IsMoreDataPending() == true);
    if (tClient.IsMoreDataPending()) tClient >> sAnswer;
    REQUIRE(sAnswer.find("foo bar") != std::string::npos);

    sAnswer.clear();
    tAnswer.clear();
    tClient.Disconnect();
    REQUIRE (tClient.Connect(sHost) == true);
    tClient << tRequest;
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    REQUIRE(tClient.IsMoreDataPending() == true);
    if (tClient.IsMoreDataPending()) tClient >> tAnswer;
    sAnswer.assign(tAnswer.begin(), tAnswer.end());
    REQUIRE(sAnswer.find("foo bar") != std::string::npos);

    //tests when not connected
    tClient.Disconnect();
    REQUIRE (tClient.IsConnected() == false);
    REQUIRE (tClient.IsMoreDataPending() == false);
    REQUIRE (tClient.Send(sRequest) == 0);
    REQUIRE (tClient.IsMoreDataPending() == false);
    REQUIRE (tClient.ReceiveString_to(1).empty());
    REQUIRE (tClient.ReceiveVector_to(1).empty());
}
//-----------------------------------------------------------------------------------------
TEST_CASE( "Test UDP socket client", "Basic udp class functionality test" )
{
    std::cout << "Start udp client unit tests" << std::endl;

    //testserver_udp is a simple echo server provided by the test environment
    std::string sHost("testserver_udp");
    std::string sPort("2001");
    std::string sRequest("Hello, World!");
    std::string sAnswer;

    // Create new client
    libsock::udp::CUDPClient tClient;
    REQUIRE (tClient.ConnectToHost(sHost, sPort) == true);

    REQUIRE (tClient.IsConnected() == true);
    REQUIRE (tClient.Send(sRequest) == (int)sRequest.size());
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    REQUIRE(tClient.IsMoreDataPending() == true);
    //Receive and check answer
    sAnswer.clear();
	sAnswer.assign(tClient.ReceiveString_to(5));
    REQUIRE(sAnswer.find("Hello, World!") != std::string::npos);

    //Send message with static function
    int numbytes = libsock::udp::CUDPClient::SendTo(sHost, sPort, sRequest.c_str(), sRequest.size());
    REQUIRE(numbytes == (int)sRequest.size());
    //***TODO: test static functions better/at all

    tClient.Disconnect();
}//-----------------------------------------------------------------------------------------
