#!/usr/bin/env python3
import socket

def run_program(a_sLocalIP:str, a_nLocalPort: int):
    bufferSize  = 1024

    # Create and bin socket
    tUDPSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
    tUDPSocket.bind((a_sLocalIP, a_nLocalPort))
    print(f"UDP echo server listening on {a_sLocalIP}:{a_nLocalPort}")

    # Listen for incoming datagrams and echo data back to client
    while(True):
        tRecvd = tUDPSocket.recvfrom(bufferSize)
        sMessage = tRecvd[0]
        sAddress = tRecvd[1]
        print(f"Received message:{sMessage} from client {sAddress}")
        # Echo back
        tUDPSocket.sendto(sMessage, sAddress)

if __name__ == '__main__':
    run_program('', 2001)
