# How to use 

## 1. Start build environment

 * cd into ```./buildenv```
 * run ```./start_dev_env.sh```

## 2. Start test servers for unit tests

 * From this directory, run ```docker-compose up```

