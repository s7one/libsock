#!/usr/bin/env bash

if ! (docker image inspect libsock_build:latest > /dev/null); then 
  echo "Building image..."
  docker image build --tag libsock_build:latest .
fi

if ! (docker network inspect libsock_unittest > /dev/null); then 
  echo "Creating network libsock_unittest"
  docker network create libsock_unittest
fi

echo "Starting build environment. Source directory is mapped to /ext."
docker run --rm -it -v ${PWD}/../../..:/ext --network libsock_unittest libsock_build:latest /bin/bash

