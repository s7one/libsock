/*
 libsock tcp server example

 Licensed under the MIT License <http://opensource.org/licenses/MIT>.
 SPDX-License-Identifier: MIT
 Copyright (c) 2018 Christoph Stein <christoph@oss.impulse9.de>

 Permission is hereby  granted, free of charge, to any  person obtaining a copy
 of this software and associated  documentation files (the "Software"), to deal
 in the Software  without restriction, including without  limitation the rights
 to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
 copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
 furnished to do so, subject to the following conditions:
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
 IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
 FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
 AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
 LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */
//------------------------------------------------------------------------------------------
#include <iostream>
#include <libsock/tcpserver.h>
#include <libsock/tcpsslclient.h>
//------------------------------------------------------------------------------------------
int main()
{
  //really simple echo server
  libsock::tcp::CTCPServer server(std::string("7000"),
                                  [](int s, const std::string& tStr)
                                    {
                                      libsock::tcp::CTCPSSLClient c(s, "./server.crt", "./server.key");
                                      c.Send("Hello, SSL!\n");
                                      while(c.Send(c.ReceiveVector()));
                                    },
                                  [](const std::string& tStr){ std::cout << tStr << std::endl;});

  //start server, will only return on error
  server.Listen();

  //error occured
  std::cout << "Error: "<< server.GetLastError() << std::endl;
}
//------------------------------------------------------------------------------------------
