# Tcp ssl echo server example

This is, in a nutshell, a normal tcp server. Once the tcp connection is established, the cTCPSSLClient class takes over and initiates the ssl handshake.

## Build and run

 * Generate self signed cert and key (see section below)
 * run *make*
 * run *./main*
 * Use *openssl* (see below) or any other program to connect to the server


## Generate self signed server certificate

You can use the following command to generate a simple self-signed certificate and key for testing purposes

```
openssl req -x509 -newkey rsa:2048 -keyout key.pem -out cert.pem -days 365 -nodes
```

This will generate the files needed by the sample program in this directory.

## Connect to example server program

You can use the openssl tool to test the connection (server sample program must be running on the same machine)

```
openssl s_client -connect localhost:7000
```
