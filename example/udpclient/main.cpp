/*
 libsock udp client example

 Licensed under the MIT License <http://opensource.org/licenses/MIT>.
 SPDX-License-Identifier: MIT
 Copyright (c) 2021 Christoph Stein <christoph@oss.impulse9.de>

 Permission is hereby  granted, free of charge, to any  person obtaining a copy
 of this software and associated  documentation files (the "Software"), to deal
 in the Software  without restriction, including without  limitation the rights
 to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
 copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
 furnished to do so, subject to the following conditions:
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
 IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
 FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
 AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
 LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */
//------------------------------------------------------------------------------------------
#include <iostream>
#include <libsock/udpclient.h>
//------------------------------------------------------------------------------------------
int main()
{
 //really simple client example
 libsock::udp::CUDPClient tClient;

  if (!tClient.ConnectToHost("localhost", "2001"))
  {
    std::cout << "Error: ConnectToHost" << std::endl;
    return 1;
  }

  std::string sRequest("Hello, World!");
  if (static_cast<unsigned int>(tClient.Send(sRequest)) != sRequest.size())
  {
    std::cout << "Error: Send" << std::endl;
    return 1;
  }

  std::string sAnswer = tClient.ReceiveString_to(5);
  std::cout << sAnswer << std::endl;

  return 0;

// *** static functions examples ***

//  // *** receive one udp packet on port 4242, then exit ***
//  char buffer[1024];
//  std::string remote;
//  unsigned int port;
//  int numbytes;
//
//  numbytes = libsock::udp::CUDPClient::RecvFrom("4242",
//                                                buffer,
//                                                sizeof(buffer),
//                                                remote,
//                                                port);
//
//  if (numbytes > (sizeof(buffer) - 1)) numbytes = sizeof(buffer) - 1;
//  buffer[numbytes] = '\0';
//  std::cout << "Received data from: " << remote << ":" << port << std::endl;
//  std::cout << buffer << std::endl;

// // *** send one udp packet localhost, port 2001 ***
// std::string sMessage = "Hello, world!";
// int numbytes = libsock::udp::CUDPClient::SendTo("localhost",
//                                                 "2001",
//                                                 sMessage.c_str(),
//                                                 sMessage.size());
//
// std::cout << "Sent " << numbytes << " bytes" << std::endl;
}
//------------------------------------------------------------------------------------------
