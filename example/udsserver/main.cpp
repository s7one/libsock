/*
 libsock unix domain socket server example

 Licensed under the MIT License <http://opensource.org/licenses/MIT>.
 SPDX-License-Identifier: MIT
 Copyright (c) 2018 Christoph Stein <christoph@oss.impulse9.de>

 Permission is hereby  granted, free of charge, to any  person obtaining a copy
 of this software and associated  documentation files (the "Software"), to deal
 in the Software  without restriction, including without  limitation the rights
 to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
 copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
 furnished to do so, subject to the following conditions:
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
 IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
 FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
 AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
 LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */
//------------------------------------------------------------------------------------------
#include <iostream>
#include <unistd.h>
#include <sys/socket.h>
#include "libsock/udsserver.h"
#include "libsock/udsclient.h"
//------------------------------------------------------------------------------------------
int main(int argc, char *argv[])
{
  if (argc < 2)
  {
    std::cout << "Usage: " << argv[0] << " <socket file path>" << std::endl;
    return 1;
  }

  // simple echo server
  libsock::uds::CUDSServer tServer(argv[1],
                                  [](int nSocket)
                                  {
                                    libsock::uds::CUDSClient tClient(nSocket);

                                    while (1)
                                    {
                                      std::string stemp = tClient.ReceiveString();
                                      if (stemp.empty())
                                      {
                                        tClient.Disconnect();
                                        return;
                                      }
                                      tClient.Send(stemp);
                                    }
                                  },
                                  [](const std::string &a_sMsg)
                                  {
                                    std::cout << a_sMsg << std::endl;
                                  });

  tServer.Listen();

	return 0;
}
//------------------------------------------------------------------------------------------
