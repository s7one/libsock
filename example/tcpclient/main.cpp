/*
 libsock tcp client example

 Licensed under the MIT License <http://opensource.org/licenses/MIT>.
 SPDX-License-Identifier: MIT
 Copyright (c) 2018 Christoph Stein <christoph@oss.impulse9.de>

 Permission is hereby  granted, free of charge, to any  person obtaining a copy
 of this software and associated  documentation files (the "Software"), to deal
 in the Software  without restriction, including without  limitation the rights
 to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
 copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
 furnished to do so, subject to the following conditions:
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
 IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
 FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
 AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
 LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */
//------------------------------------------------------------------------------------------
#include <iostream>
#include <libsock/tcpclient.h>
//------------------------------------------------------------------------------------------
int main()
{
  //really simple client example
  libsock::tcp::CTCPClient tClient;

  if (!tClient.ConnectToHost("www.example.com", "80"))
  {
    std::cout << "Error: ConnectToHost" << std::endl;
    return 1;
  }

  std::string sRequest("GET / HTTP/1.1\r\nHost: ");
  sRequest.append("www.example.com");
  sRequest.append(std::string("\r\nConnection: close\r\n\r\n"));

  if (static_cast<unsigned int>(tClient.Send(sRequest)) != sRequest.size())
  {
    std::cout << "Error: Send" << std::endl;
    return 1;
  }

  std::string sAnswer = tClient.ReceiveString_to(5);
  std::cout << sAnswer << std::endl;

}
//------------------------------------------------------------------------------------------
